;;     ________    __                                __
;;    / ____/ /___/ /___        ____ ___  ____  ____/ /__
;;   / __/ / / __  / __ \______/ __ `__ \/ __ \/ __  / _ \
;;  / /___/ / /_/ / /_/ /_____/ / / / / / /_/ / /_/ /  __/
;; /_____/_/\__,_/\____/     /_/ /_/ /_/\____/\__,_/\___/
;;
;; eldo-mode.el --- major mode for Eldo netlist files
;;
;; Authors:    Emmanuel Rouat <emmanuel.rouat@st.com>
;;             Geert A. M. Van der Plas  <geert_vanderplas@email.com>
;;             Carlin J. Vieri, MIT AI Lab <cvieri@ai.mit.edu> 1994
;              Aurelien Buchet (comment hide/code folding)
;; Maintainer: Emmanuel Rouat <emmanuel.rouat@st.com>
;; Version: 2.0
;; URL: http://www.esat.kuleuven.ac.be/~vdplas/emacs/
;; Keywords: eldo,spice,analog simulator,netlist
;; Compatibility: Emacs29
;;
;; Copyright © 1997-2025 Emmanuel Rouat <emmanuel.rouat@st.com>
;; Copyright © 2000-2002 Geert A. M. Van der Plas <geert_vanderplas@email.com>
;;
;; Eldo is a product of Siemens™ (former Mentor Graphics)
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.



;;-----------------------------------------------------------------------------
;; COMMENTARY:
;;-----------------------------------------------------------------------------
;;
;; This major mode for emacs provides support for editing ELDO netlist files.

;; You may wish to add something like the following to your ~/.emacs file:
;; (autoload 'eldo-mode "eldo-mode" nil t)
;; (setq auto-mode-alist (cons '("\\.cir$"  . eldo-mode) auto-mode-alist))
;; (setq auto-mode-alist (cons '("\\.ckt$"  . eldo-mode) auto-mode-alist))
;;
;; Put this file in the emacs load-path so emacs can find it (check the manual).
;; (I put it in the dedicated ~/.emacs.d/lisp directory)
;; This mode will activate automatically on files which end by .ckt or .cir
;; This mode requires font-lock, easymenu, imenu and htmlize
;;
;; Most of the code of this mode was shamelessly stolen from vhdl-mode (ron whitby)
;; and vhdl-electric mode - and from an emacs mode using 'hilit, written at Anacad
;; Also some borrowings from tcl-mode and zillions of .el files
;; Functions/subckt support from spice-mode by C.J Vieri
;; Empty file initialize inspired by rpm-spec-mode (Stig Bjørlykke)
;; (also Changelog support)
;; 'Eldo-colorized-libraries' based on code by Frederic MIENVILLE
;; Some regexps/ideas were taken/shared with Geert Van Der Plas' spice-mode
;; ( see http://www.esat.kuleuven.ac.be/~vdplas/emacs/ )
;;
;; Good they were there... I HATE ELISP!!!!!
;;
;;-----------------------------------------------------------------------------
;; TODO:
;;-----------------------------------------------------------------------------
;;
;; - more effective/easier templates (use skeleton ?)
;; - use xref for subckt definitions search?
;;
;;-----------------------------------------------------------------------------
;; BUGS:
;;-----------------------------------------------------------------------------
;;
;; - sometimes in case of multi-line instantiation the instance name
;;   fontification fails. In this case, try 'reformat netlist'.
;;
;;-----------------------------------------------------------------------------



;;-----------------------------------------------------------------------------
;; Misc variables/constants
;;-----------------------------------------------------------------------------

(defconst eldo-mode-version "2.0 February 2025"
  "Current version of eldo mode.")

(defconst eldo-mode-developer
  "Emmanuel Rouat <emmanuel.rouat@st.com>"
  "Current developer/maintainer of eldo-mode.")

(defvar eldo-mode-hook nil
  "Function or functions to run on entry to eldo-mode.")

(defvar eldo-end-comment-column (1- fill-column)
  "*Last column in Eldo comment (before line wraps)")

(defvar eldo-tempo-tags nil
  "List of templates used in Eldo mode.")

(defun eldo-about ()
  (interactive)
  (sit-for 0)
  (message "Eldo-mode version %s, © %s" eldo-mode-version eldo-mode-developer))


;;-----------------------------------------------------------------------------
;; Customization support
;;-----------------------------------------------------------------------------

(defgroup eldo-mode nil
  "Customizations for Eldo Mode."
  :prefix "eldo-mode-"
  :group 'languages)

(defgroup eldo-faces nil
  "Customizations for highlighting."
  :group 'eldo-mode)

(defcustom eldo-use-imenu t
  "*Non-nil means use imenu to create an index menu in the menubar.
This index will list subcircuits, bipolar, mosfet and diode models."
  :type 'boolean
  :group 'eldo-mode)

(defun eldo-customize ()
  "Call the customize function with `eldo-mode' as argument."
  (interactive)
  (customize-browse 'eldo-mode))


;;-----------------------------------------------------------------------------
;; Required packages
;;-----------------------------------------------------------------------------

(require 'font-lock)
(require 'tempo)
(require 'imenu)
(require 'easymenu)
(require 'skeleton)
(require 'htmlize)

;;-----------------------------------------------------------------------------
;; Key bindings
;;-----------------------------------------------------------------------------

(defvar eldo-mode-map ()
  "Keymap containing eldo commands.")

(if eldo-mode-map
  ()
  (setq eldo-mode-map (make-sparse-keymap))

  (define-key eldo-mode-map (kbd "<return>")  'newline-and-indent-same-level)

  (define-key eldo-mode-map (kbd "<tab>")     'eldo-tempo-complete-or-indent)
  (define-key eldo-mode-map (kbd "<backtab>") 'eldo-unindent)
  ;(define-key eldo-mode-map (kbd "<backtab>") (lambda() (interactive) (shift-region -4)))

  ;; key bindings (start with Control-C)
  ;; inspired by vhdl-mode for some compatibility

  (define-key eldo-mode-map (kbd "C-c C-b") 'eldo-reformat-netlist)
  (define-key eldo-mode-map (kbd "C-c C-f") 'font-lock-fontify-buffer)

  ;; key bindings for include file loading
  (define-key eldo-mode-map (kbd "C-c l")   'eldo-load-include-files)
  (define-key eldo-mode-map (kbd "C-c rl")  'eldo-load-include-files-recursively)

  ;; key bindings for comment/uncomment
  (define-key eldo-mode-map (kbd "C-c c")   'eldo-comment-region-or-line)
  (define-key eldo-mode-map (kbd "C-c u")   'eldo-uncomment-region-or-line)
  (define-key eldo-mode-map (kbd "C-c C-c") 'eldo-comment-uncomment-region-or-line)

  ;; key bindings for hiding/folding
  (define-key eldo-mode-map (kbd "C-c h")   'eldo-hide-comments)
  (define-key eldo-mode-map (kbd "C-c sh")  'eldo-show-comments)
  (define-key eldo-mode-map (kbd "C-c f")   'eldo-fold-subckts)
  (define-key eldo-mode-map (kbd "C-c sf")  'eldo-show-subckts)
  (define-key eldo-mode-map (kbd "C-c sa")  'eldo-show-all)

  ;; changelog addition
  (define-key eldo-mode-map (kbd "C-c ac")  'eldo-add-changelog-entry)

  ;; mouse bindings
  (define-key eldo-mode-map [(mouse-3)] 'ffap-at-mouse))


;;-----------------------------------------------------------------------------
;; Eldo-mode menu (using `easy-menu.el')
;;-----------------------------------------------------------------------------

(defun eldo-create-mode-menu ()
  "Create ELDO Mode menu."
  (list
   "Eldo"
    "------"
   '("Edit"
     ["Add file header"         (concat (goto-char (point-min)) (eldo-default-header) t)]
     ["Comment Region"          eldo-comment-region-or-line (mark)]
     ["Uncomment Region"        eldo-uncomment-region-or-line (mark)]
     ["(Un)comment Region"      eldo-comment-uncomment-region-or-line (mark)]
     "--"
     ["Fontify"                 font-lock-fontify-buffer t]
     ["Reformat netlist"        eldo-reformat-netlist t]
     ["Show buffer as html"     htmlize-buffer t]
     ["Show region as html"     htmlize-region t]
     "--"
     ["Add subckt"              tempo-template-eldo-subckt t]
     ["Add section"             eldo-add-section t]
     ["Add section bar"         eldo-section-bar t]
     "--"
     ["Load include/lib files"  eldo-load-include-files  t]
     ["Same but recursively"    eldo-load-include-files-recursively t]
     ["Unload all other Eldo files" eldo-unload-other-decks t]
     "--"
     ["Add Changelog Entry"     eldo-add-changelog-entry t]
     )
   '("Hide/Fold"
     ["Hide comments"           eldo-hide-comments t]
     ["Show comments"           eldo-show-comments t]
     ["Fold subckts"            eldo-fold-subckts t]
     ["Unfold subckts"          eldo-show-subckts t]
     ["Show all"                eldo-show-all t]
     )
   '("Add Elements"
     ("Passive elements"
      ["Resistor"               tempo-template-eldo-resistor t]
      ["Capacitor"              tempo-template-eldo-capacitor t]
      ["Inductor"               tempo-template-eldo-inductor t]
      ["RC line"                tempo-template-eldo-rcline t]
      )
     ("Active elements"
      ["Diode"                  tempo-template-eldo-diode t]
      ["Bipolar"                tempo-template-eldo-bipolar t]
      ["Jfet"                   tempo-template-eldo-jfet t]
      ["Mosfet"                 tempo-template-eldo-mosfet t]
      )
     ("Voltage Sources"
      ["Voltage controlled"     tempo-template-eldo-vcvs t]
      ["Current controlled"     tempo-template-eldo-ccvs t]
       )
     ("Current Sources"
       ["Voltage controlled"    tempo-template-eldo-vccs t]
       ["Current controlled"    tempo-template-eldo-cccs t]
       )
     ("Waveforms"
       ["PWL"                   (eldo-pwl) t]
       ["pulse"                 tempo-template-eldo-pulse t]
       ["ac"                    tempo-template-eldo-ac t]
       ["am"                    tempo-template-eldo-am t]
       ["sin"                   tempo-template-eldo-sine t]
       ["sffm"                  tempo-template-eldo-sffm t]
       ["exp"                   tempo-template-eldo-exp t]
       ["noise"                 tempo-template-eldo-noise t]
       ["pattern"               tempo-template-eldo-pattern t]
       )
     ("Macromodels"
      ("Analog"
       ["SO Comparator"         tempo-template-eldo-comp t]
       ["DO Comparator"         tempo-template-eldo-compd t]
       ["SO Linear Opamp"       tempo-template-eldo-linear-opa0 t]
       ["DO Linear Opamp"       tempo-template-eldo-linear-opa0d t]
       ["SO Linear 1-pole Opamp"    tempo-template-eldo-linear-opa1 t]
       ["DO Linear 1-pole Opamp"    tempo-template-eldo-linear-opa1d t]
       ["SO Linear 2-pole Opamp"    tempo-template-eldo-linear-opa2 t]
       ["DO Linear 2-pole Opamp"    tempo-template-eldo-linear-opa2d t]
       ["Delay"                 tempo-template-eldo-delay t]
       ["Saturating Resistor"   tempo-template-eldo-satr t]
       ["Voltage Limiter"       tempo-template-eldo-satv t]
       ["Voltage cont. switch"  tempo-template-eldo-vswitch t]
       ["Current cont. switch"  tempo-template-eldo-cswitch t]
       ["Triangular to sine converter"  tempo-template-eldo-tri2sin t]
       ["Staircase generator"   tempo-template-eldo-stairgen t]
       ["Sawtooth generator"    tempo-template-eldo-sawgen t]
       ["Triangle generator"    tempo-template-eldo-trigen t]
       ["Amplitude modulator"   tempo-template-eldo-amm t]
       ["Pulse amplitude modulator" tempo-template-eldo-pam t]
       ["Sample&Hold"           tempo-template-eldo-saho t]
       ["Track&Hold"            tempo-template-eldo-trho t]
       ["Peak Detector"         tempo-template-eldo-peakd t]
       ["SO Level Detector"     tempo-template-eldo-levdso t]
       ["DO Level Detector"     tempo-template-eldo-levddo t]
       ["Log Amplifier"         tempo-template-eldo-logamp t]
       ["Antilog Amplifier"     tempo-template-eldo-antilog t]
       ["Differentiator"        tempo-template-eldo-diff t]
       ["Integrator"            tempo-template-eldo-integ t]
       ["Add/Sub/Mult/Div"      tempo-template-eldo-adder t]
       )
      ("Digital"
       ["Delay"                 tempo-template-eldo-delay t]
       ["Inverter"              tempo-template-eldo-inv t]
       ["XOR gate"              tempo-template-eldo-xor t]
       ["2 input AND gate"      tempo-template-eldo-and2 t]
       ["2 input NAND gate"     tempo-template-eldo-nand2 t]
       ["2 input OR gate"       tempo-template-eldo-or2 t]
       ["2 input NOR gate"      tempo-template-eldo-nor2 t]
       ["3 input AND gate"      tempo-template-eldo-and3 t]
       ["3 input NAND gate"     tempo-template-eldo-nand3 t]
       ["3 input OR gate"       tempo-template-eldo-or3 t]
       ["3 input NOR gate"      tempo-template-eldo-nor3 t]
       )
      ("Mixed Signal"
       ["AD Converter"          tempo-template-eldo-adc t]
       ["DA Converter"          tempo-template-eldo-dac t]
       )
      ("Switched Cap"
       ["Opamp"                 tempo-template-eldo-switchcap-opa t]
       ["Switch"                tempo-template-eldo-switch t]
       )
      )
     )
   '("Extracts"
      ("LSTB"
        ["Phase margin"         tempo-template-eldo-phmag t]
        ["PM min"               tempo-template-eldo-pmmin t]
        ["Gain margin"          tempo-template-eldo-gmag t]
        ["Cut freq."            tempo-template-eldo-fc t]
        ["Unity gain freq."     tempo-template-eldo-ugfc t]
        )
      ("Bandgap"
        ["Flatness"             tempo-template-eldo-bandgap-flatness t]
      )
     ["Signal period"           tempo-template-eldo-period t]
     )
   '("Macros"
      ["Signal period"          tempo-template-eldo-period-macro t]
      ["Signal Duty Cycle"      tempo-template-eldo-duty-macro t]
      ["Signal settling time"   tempo-template-eldo-settling-macro t]
     )
    "------"
     ["Font-lock"               font-lock-mode :style toggle :selected font-lock-mode]
     ["Auto fill"               auto-fill-mode :style toggle :selected auto-fill-function]
     ["Outline mode"            outline-minor-mode :style toggle :selected outline-minor-mode]
     ["Which function"          which-function-mode :style toggle :selected which-function-mode]
     ["Reveal mode"             reveal-mode :style toggle :selected reveal-mode]
    "------"
     ["Customize..."            eldo-customize t]
    ["About Eldo-Mode"          eldo-about t]))

(defvar eldo-menu-list nil
  "Eldo Mode menu.")

(defun eldo-update-mode-menu ()
  "Update Eldo mode menu."
  (interactive)
  (setq eldo-mode-menu-list (eldo-create-mode-menu))
  (easy-menu-define eldo-menu eldo-mode-map "Menu keymap for Eldo Mode." eldo-menu-list))


;;-----------------------------------------------------------------------------
;; Eldo mode syntax table
;;-----------------------------------------------------------------------------

;; In eldo you can also make a block comment, with
;; a #c (begin comment) and #e (end comment)

(defvar eldo-mode-syntax-table nil
  "The syntax table used in `eldo-mode' buffers")

(if eldo-mode-syntax-table
  ()
  (setq eldo-mode-syntax-table (make-syntax-table (standard-syntax-table)))
  (modify-syntax-entry ?\n "> b" eldo-mode-syntax-table)
  (modify-syntax-entry ?! "w"    eldo-mode-syntax-table)
  (modify-syntax-entry ?# "w 13" eldo-mode-syntax-table)
  (modify-syntax-entry ?c "w 2"  eldo-mode-syntax-table)
  (modify-syntax-entry ?e "w 4"  eldo-mode-syntax-table)
  (modify-syntax-entry ?/ "w"    eldo-mode-syntax-table)
  (modify-syntax-entry ?' "w"    eldo-mode-syntax-table)
  (modify-syntax-entry ?. "w"    eldo-mode-syntax-table)
  (modify-syntax-entry ?, "w"    eldo-mode-syntax-table)
  (modify-syntax-entry ?_ "w"    eldo-mode-syntax-table)
  (modify-syntax-entry ?- "w"    eldo-mode-syntax-table)
  (modify-syntax-entry ?@ "w"    eldo-mode-syntax-table)
  (modify-syntax-entry ?: "w"    eldo-mode-syntax-table)
  (modify-syntax-entry ?\[ "("   eldo-mode-syntax-table)
  (modify-syntax-entry ?\] ")"   eldo-mode-syntax-table)
  (modify-syntax-entry ?< "w"    eldo-mode-syntax-table)
  (modify-syntax-entry ?> "w"    eldo-mode-syntax-table)
  (modify-syntax-entry ?= "."    eldo-mode-syntax-table)
  (set-syntax-table eldo-mode-syntax-table))

;; Note: in eldo syntax, nearly all caracters are valid words!
;; For instance a model can be called BIP_TYPICAL#1.2[foo]
;; This is why nearly all caracters here have word-class


;;-----------------------------------------------------------------------------
;; Font-lock section
;;-----------------------------------------------------------------------------

;; ELDO is case-insensitive
(put 'eldo-mode 'font-lock-keywords-case-fold-search t)


(custom-add-to-group
 'eldo-faces 'font-lock-comment-face 'custom-face)
(custom-add-to-group
 'eldo-faces 'font-lock-keyword-face 'custom-face)
(custom-add-to-group
 'eldo-faces 'font-lock-type-face 'custom-face)
(custom-add-to-group
 'eldo-faces 'font-lock-function-name-face 'custom-face)
(custom-add-to-group
 'eldo-faces 'font-lock-variable-name-face 'custom-face)


(defvar eldo-title-face 'eldo-title-face
  "Face name to  highlight eldo netlist title lines.")

(defface eldo-title-face
  '((((class color) (background light)) (:foreground "Red" :bold t))
    (((class color) (background dark))  (:foreground "Red" :bold t)))
  "Face name to highlight eldo netlist title lines."
  :group 'eldo-faces)

(defvar eldo-doc-face   'eldo-doc-face
  "Face name to highlight eldo netlist documentation.")

(defface eldo-doc-face
  '((((class color) (background light)) (:foreground "Blue1"))
    (((class color) (background dark))  (:foreground "Blue1")))
  "Face name to highlight eldo netlist documentation."
  :group 'eldo-faces)

(defvar eldo-analysis-face 'eldo-analysis-face
  "Face name to highlight eldo analysis commands.")

(defface eldo-analysis-face
  '((((class color) (background light)) (:foreground "Red2" :bold t))
    (((class color) (background dark))  (:foreground "Red2" :bold t)))
  "Eldo mode face used to highlight analysis commands."
  :group 'eldo-faces)

(defvar eldo-output-face 'eldo-output-face
  "Face name to highlight eldo output commands.")

(defface eldo-output-face
  '((((class color) (background light)) (:foreground "Purple2" :bold t))
    (((class color) (background dark))  (:foreground "Purple2" :bold t)))
  "Eldo mode face used to highlight output commands."
  :group 'eldo-faces)

(defvar eldo-instance-name-face 'eldo-instance-name-face
  "Face name to highlight eldo instance names.")

(defface eldo-instance-name-face
  '((((class color) (background light)) (:foreground "Medium Blue" :bold t))
    (((class color) (background dark))  (:foreground "Medium Blue" :bold t)))
  "Eldo mode face used to highlight instance names."
  :group 'eldo-faces)

(defvar eldo-model-name-face 'eldo-model-name-face
  "Face name to highlight model names in instanciations.")

(defface eldo-model-name-face
  '((((class color) (background light)) (:foreground "Red3"))
    (((class color) (background dark)) (:foreground "Red3")))
  "Eldo mode face used to highlight model names in instanciations."
  :group 'eldo-faces)

(defvar eldo-testbench-face 'eldo-testbench-face
  "Face name to highlight eldo testbench or task commands.")

(defface eldo-testbench-face
  '((((class color) (background light)) (:foreground "DodgerBlue" :bold t))
    (((class color) (background dark))  (:foreground "DodgerBlue" :bold t)))
  "Eldo mode face used to highlight testbench or tasks commands."
  :group 'eldo-faces)

(defvar eldo-bracketed-expression-face 'eldo-bracketed-expression-face
  "Face name to highlight eldo expressions between curly braces.")

(defface eldo-bracketed-expression-face
  '((((class color) (background light)) (:foreground "DodgerBlue3"))
    (((class color) (background dark))  (:foreground "DodgerBlue3")))
  "Eldo mode face used to highlight expressions between curly braces."
  :group 'eldo-faces)

(defvar eldo-units-face 'eldo-units-face
  "Face name to highlight eldo reserved unit names.")

(defface eldo-units-face
  '((((class color) (background light)) (:foreground "DarkOrchid1" :bold t :italic t))
    (((class color) (background dark))  (:foreground "DarkOrchid1" :bold t :italic t)))
  "Eldo mode face used to highlight reserved unit names."
  :group 'eldo-faces)

(defvar eldo-operators-face 'eldo-operators-face
  "Face name to highlight eldo operators (usually in extract commands).")

(defface eldo-operators-face
  '((((class color) (background light)) (:foreground "CadetBlue" :bold t))
    (((class color) (background dark))  (:foreground "CadetBlue" :bold t)))
  "Eldo mode face used to highlight operators (usually in extract commands)."
  :group 'eldo-faces)

(defface eldo-hidden-face
  '((t (:weight bold :foreground "DarkBlue")))
;;   '((t (:weight bold :foreground "cyan" :background "darkolivegreen")))
  "Eldo mode face for hidden text overlay."
  :group 'eldo-faces)


;;-----------------------------------------------------------------------------
;; List of Eldo keywords/syntax - order is important!!
;;-----------------------------------------------------------------------------

(defconst eldo-model-name "\\([a-z]\\sw+[^ \t\n=]*\\)"
  "Regexp that describes a syntactically correct model or subckt name")

(defconst eldo-instance-name "\\([xmdq][^ \t\n]+\\)"
  "Regexp that describes a syntactically correct subckt/mosfet/diode/bipolar instance name")

(defconst eldo-line-break "\\(\n\\s-*\\+\\s-*\\)*"
  "Regexp that matches a (possible) line break (\n+)")

(defconst eldo-library-regexp
  "^\\s-*[!*]*\\.\\(include\\|lib\\|libfas\\)\\s-+\\(?:key=\\w+\\s-\\)*"
  "Regexp that matches the beginning of library filename")

(defconst eldo-xinstance-regexp
    (concat
     "^\\s-*" eldo-instance-name
     "\\(\\([ \t]+[^ *!(=\t\n][^ (=\t\n]*\\|[ \t]*\\(\n?[*!].*\\)?\n[+]\\)*\\s-*\\)"
     "\\<" eldo-model-name "\\>"
     "\\(\\s-*\n\\|\\s-+[^=\n]\\)"
     )
  "Regexp for x instances.")

(defconst eldo-analysis-keywords
  '(
    "ac" "age"
    "checksoa" "conso" "current_analysis"
    "dc" "dchiz" "dcmismatch"
    "hiz" "four" "lstb" "mc"
    "noise" "noisetran" "op" "optimize" "pz"
    "sens" "snf" "sst" "sstac" "sstnoise" "step"
    "temp" "tf" "tran" "wcase"
    )
  "List of Eldo analysis type keywords")

(defconst eldo-model-keywords
  '(
    "cap " "c" "d" "ind" "res" "r"
    "npn" "pnp" "lpnp"
    "nmos" "pmos" "njf" "pjf" "nsw" "psw"
    "opa" "modfas" "logic" "a2d" "d2a"
    "macro" "hook"
    )
  "List of Eldo models")

(defconst eldo-macromodel-keywords
  '(
    "opamp0" "opamp0d" "opamp1" "opamp1d" "opamp2" "opamp2d"
    "satr" "satv" "vswitch" "cswitch"
    "tri2sin" "stairgen" "sawgen" "trigen"
    "amm" "pam"
    "sa_ho" "tr_ho"
    "pwm" "vco"
    "peak_d" "lev_d"
    "logamp" "expamp"
    "diff" "integ"
    "add" "sub" "mult" "div"
    "sc_ideal" "sc_i" "sc_n" "sc_p" "sc_s1" "sc_s2"
    "sc_sp1" "sc_sp2" "sc_b" "sc_u"
    )
  "List of Eldo macromodels")

(defconst eldo-commands
  '(
    "a2d" "addlib" "alter"
    "bind"
    "call_tcl" "checkbus" "chrand" "chrent"
    "chrsim" "comchar" "connect" "correl"
    "d2a" "data" "defhook" "define_function" "define_testbench" "define_task"
    "defmac" "defplotdig" "defwave" "discard"
    "del" "distrib" "disflat" "dspf_include"
    "end" "endif" "ends" "enddata" "endl" "end_define_function" "end_define_testbench" "end_define_task"
    "else" "elseif" "extmod" "extract"
    "ffile" "func"
    "global" "guess"
    "hier" "histogram"
    "ic" "if" "include" "init" "iprobe"
    "lib" "libfas" "load" "loop" "lotgroup" "localtol"
    "mcmod" "moddup" "model" "modlogic" "monitor" "meas"
    "newpage" "nocom" "nodeset" "notrc"
    "objective" "optfour" "option" "options"
    "optnoise" "optpwl" "optwind"
    "param" "paramopt" "plotbus" "plotwind" "printbus" "printfile" "probe" "protect"
    "ramp" "restart" "rfblock"
    "save" "setbus" "setsoa" "sigbus"
    "sinus" "solve" "subdup"
    "sstprobe"
    "table" "temp" "title" "topcell" "tvinclude"
    "use" "use_tcl" "unprotect"
    "verilog"
    "width"
    )
  "List of Eldo commands (dot cards)")

(defconst eldo-keywords
  '(
    "ac" "dc" "exp" "guess" "ic"
    "list" "nodeset" "nonoise" "overwrite_input"
    "param" "pattern" "pulse" "pwl"
    "sin" "sweep" "table" "temp"
    "when" "fall"
    "port" "generic"    ;;vhdl-ams stuff
    )
  "List of Eldo reserved keywords (that are not commands)")

(defconst eldo-operators  ;; used in extracts
  '(
    "average" "compress" "crossing" "d_wa" "dcm" "delta" "disto" "dtc"
    "eval" "fail" "falling" "integ" "k_factor" "local_max" "local_min"
    "maxgmvt" "max" "mean" "min" "modpar" "opmode" "pass" "pval"
    "rising" "rms" "slewrate" "slope" "tcross" "tfall" "tinteg"
    "tpd" "tpddd" "tpdud" "tpddu" "tpduu" "tperiod" "trise"
    "valat" "vdsatc" "vdsatgds" "vtc" "wfreq" "winteg"
    "xcompress" "xdown" "xmax" "xmin" "xthres" "xup" "xycond" "yval"
    "abs" "bus" "meas" "pval" "sign" "slope" "vdig"
    "catvect" "vect"
    )
  "List of Eldo operators (usually used in extracts)")

(defconst eldo-units
  '(
    "current" "amp" "ampere"
    "charge" "coulomb"
    "capacitance" "farad"
    "inductance" "henry"
    "size" "length" "width" "meter"
    "magnitude" "db"
    "resistance" "ohm"
    "phase" "degree"
    "power" "watt"
    "frequency" "freq" "hertz" "hz"
    "temperature" "temp" "kelvin"
    "time" "second"
    "voltage" "volt"
    "flux"
    "none"
    )
  "List of Eldo reserved unit names")

(defvar  eldo-font-lock-keywords
  (list
   ;; Title line
   '("\\`.+$" 0 eldo-title-face)

   ;; doc comments - start with '!' unless global net (gnd!) used to document your netlist
   '("\\(^\\|\\s-\\)![^*\n].*" 0 eldo-doc-face t)

   ;; comments - lines starting with '*' are ignored by eldo
   '("^\\s-*\\*.*" 0 font-lock-comment-face t)

   ;; analysis - keywords like .op, .tran etc (eldo-analysis-face)
   (list (concat "^\\s-*\\.\\(" (regexp-opt eldo-analysis-keywords) "\\)\\>") '(0 eldo-analysis-face))

   ;; print,plot,extract,meas,conso,monitor,objective
   (list (concat
          "^\\s-*\\.\\(print\\|plot\\|extract\\|meas\\|monitor\\|objective\\|printfile\\)\\s-*" eldo-line-break
          (regexp-opt '( "ac" "dc" "dcac" "dcsweep" "extract" "mc" "tran" "dctran" "noisetran" "noise"
                         "four" "sweep" "opt" "fsst" "tsst" "sstac" "sstnoise" ) t )
          "\\>" ) '(0  eldo-output-face))

   ;; plotbus , probe , newpage
   '("^[ \t]*\\.\\(plotbus\\|probe\\|newpage\\|title\\)\\>" . eldo-output-face)

   ;; subckt and ends statement
   (list (concat
     "^\\s-*"
     "\\.\\(subckt\\|ends\\)"
     "\\s-+" eldo-model-name "\\>")
    '(1 font-lock-keyword-face)
    '(2 font-lock-function-name-face nil t))

   ;; testbench and ends statement
   (list (concat
     "^\\s-*"
     "\\.\\(define_testbench\\|end_define_testbench\\)"
     "\\s-+" eldo-model-name "\\>")
    '(1 font-lock-keyword-face)
    '(2 font-lock-function-name-face nil t))

   ;; task and ends statement
   (list
    (concat
     "^\\s-*"
     "\\.\\(define_task\\|end_define_task\\)"
     "\\s-+" eldo-model-name "\\>")
    '(1 font-lock-keyword-face)
    '(2 font-lock-function-name-face nil t))

   ;; model definition
   (list (concat
     "^\\s-*"
     "\\(\\.model\\)"
     "\\s-+" eldo-model-name eldo-line-break "\\s-+\\("
     (regexp-opt eldo-model-keywords)
     "\\)\\>" )
    '(1 font-lock-keyword-face)
    '(2 font-lock-function-name-face)
    '(4 font-lock-type-face))

   ;; assignments (xx=zz) - slows down fontification a lot
   '("\\(\\<\\w*\\(\\s(\\w*\\s)\\)*\\)\\s-*=" 1 font-lock-variable-name-face)

   ;; bracketed expression {...}
   '("\\({[^{]+}\\)" 1 eldo-bracketed-expression-face)

   ;; libraries, includes....
   (list (concat
     eldo-library-regexp
     "\\s-*\\<\\(\\w+\\)\\>" )
    '(1 font-lock-keyword-face)
    '(2 eldo-model-name-face nil t))

   ;; 'Y' instances (macromodels)
   (list (concat
     "^\\s-*\\(Y[^ \t\n]+\\)" eldo-line-break "\\s-+\\("
     (regexp-opt eldo-macromodel-keywords)
     "\\)\\>" )
    '(1 eldo-instance-name-face)
    '(3 eldo-model-name-face))

   ;; instances of subcircuits/mosfets/diodes
   (list eldo-xinstance-regexp '(1 eldo-instance-name-face) '(5 eldo-model-name-face))

   ;; some keywords
   (list (concat "\\<\\(" (regexp-opt eldo-keywords ) "\\)\\>" ) '(0 font-lock-keyword-face))

   ;; '.' keywords (commands)
   (list (concat "^\\s-*\\.\\(" (regexp-opt eldo-commands) "\\)\\>") '(0 font-lock-keyword-face))

  ;; highlight units
   (list (concat "\\<\\(" (regexp-opt eldo-units) "\\)\\>" ) '(0 eldo-units-face))

  ;; highlight operators
   (list (concat "[(-@ *)]\\(" (regexp-opt eldo-operators) "\\)\\>" ) '(1 eldo-operators-face))

   ;; highlight additional . unknowns (usually testbenches or tasks)
   '("^[ \t]*[\.][^ \t\n]*" 0 eldo-testbench-face)

   ;; highlight other keywords: every non fontified word that starts a line
   ;; except '+' which is line continuation, '!' that starts a doc comment
   ;; and '.' that starts a command.
  '("^[ \t]*[^+!\n.]\\w+" . eldo-instance-name-face)
   )
  "Regexp describing fontification syntax of Eldo keywords")


;;-----------------------------------------------------------------------------
;; Misc functions - indenting/commenting
;;-----------------------------------------------------------------------------

(defun newline-and-indent-same-level ()
  "Insert a newline, then indent to the same column as the current line."
  (interactive)
  (let ((col (save-excursion (back-to-indentation) (current-column))))
    (newline)
    (indent-to-column col)))

(defun eldo-tempo-complete-or-indent (beg end)
  "If region selected, indent. If not, attempt tempo completion. If unsuccessful, indent point to next tab stop."
  (interactive "r")
  (if (use-region-p)
    (save-excursion
      (indent-rigidly-right-to-tab-stop beg end)
      (setq deactivate-mark nil))
     (if (eq 'fail (tempo-complete-tag 'fail))
      (indent-for-tab-command))))

;; https://emacs.stackexchange.com/questions/12334/elisp-for-applying-command-to-only-the-selected-region
(defun eldo-indent (beg end)
  "If region selected, indent. If not, indent point to next tab stop."
  (interactive "r")
  (if (use-region-p)
    (save-excursion
      (indent-rigidly-right-to-tab-stop beg end)
      (setq deactivate-mark nil))
    (indent-for-tab-command)))

;; https://emacs.stackexchange.com/questions/32816/backwards-tab-to-tab-stop
(defun eldo-unindent (beg end)
  "If region selected, unindent. If not, indent point to previous tab stop."
  (interactive "r")
  (if (use-region-p)
    (save-excursion
      (indent-rigidly-left-to-tab-stop beg end)
      (setq deactivate-mark nil))
    (let ((nexttab (indent-next-tab-stop (current-column) t)))
      (delete-horizontal-space t)
      (indent-to nexttab))))

(defun eldo-comment-region-or-line ()
  "Comment selected region or current line."
  (interactive)
  (let (beg end)
    (if (region-active-p)
        (setq beg (region-beginning) end (region-end))
      (setq beg (line-beginning-position) end (line-end-position)))
    (save-excursion
      (goto-char beg)
      (narrow-to-region beg end)
        (while (not(eobp))
            (beginning-of-line)
            (if (not (looking-at "[[:blank:]]*$"))
                (insert comment-start))
          (forward-line 1)
        )
      (widen)
      (setq deactivate-mark nil))))

(defun eldo-uncomment-region-or-line ()
  "Uncomment selected region or current line."
  (interactive)
  (let (beg end)
    (if (region-active-p)
        (setq beg (region-beginning) end (region-end))
      (setq beg (line-beginning-position) end (line-end-position)))
    (save-excursion
      (goto-char beg)
      (narrow-to-region beg end)
        (while (not(eobp))
            (back-to-indentation)
            (if (looking-at comment-start)
                (delete-char (length comment-start))
              )
          (forward-line 1)
        )
      (widen)
      (setq deactivate-mark nil))))

(defun eldo-comment-uncomment-region-or-line ()
  "Comment/Uncomment selected region or current line (toggle-like)."
  (interactive)
  (let (beg end)
    (if (region-active-p)
        (setq beg (region-beginning) end (region-end))
      (setq beg (line-beginning-position) end (line-end-position)))
    (save-excursion
      (goto-char beg)
      (narrow-to-region beg end)
        (while (not(eobp))
            (back-to-indentation)
            (if (looking-at comment-start)
                (delete-char (length comment-start))
              (beginning-of-line)
              (if (not (looking-at "[[:blank:]]*$"))
                  (insert comment-start))
              )
          (forward-line 1)
        )
      (widen)
      (setq deactivate-mark nil))))

;; the next function could be much more complete
(defun eldo-reformat-netlist ()
  (interactive)
  (let ((pos (point)))
    (goto-char (point-min))
    (while (re-search-forward "^\\s-*\\+\\([^ ]\\)" nil t)
      (replace-match "+ \\1" ))
    (font-lock-fontify-buffer)
    (goto-char pos)))

;; (defun eldo-set-mismatch (beg end)
;;   "Set MISMATCH parameter ON for devices in selected region"
;;   (interactive "r")
;;   (goto-char beg)
;;   (narrow-to-region beg end)
;;     (while (search-forward "Mismatch=0" nil t)
;;       (replace-match "Mismatch=1" nil t ))
;;     (widen))
;;
;; (defun eldo-unset-mismatch (beg end)
;;   "Set MISMATCH parameter OFF for devices in selected region"
;;   (interactive "r")
;;   (goto-char beg)
;;   (narrow-to-region beg end)
;;     (while (search-forward "Mismatch=1" nil t)
;;       (replace-match "Mismatch=0" nil t ))
;;     (widen))

;; ===============================================================================================================
;; Functions to hide & show comments and subcircuits
;; A. Buchet - February 2025
;; ===============================================================================================================

;; Add Overlay
(defun eldo-hide-region (beg end label)
  "Hide all text between BEG and END behind an overlay."
  (let ( ( o (make-overlay beg end nil t) )
         )
    (overlay-put o 'label                   (intern label)                            )
    (overlay-put o 'evaporate               t                                         )
    (overlay-put o 'invisible               'eldo                                     )
    (if (string-equal label "eldo-comments")
        (overlay-put o 'display                 (propertize "<...>" 'face 'eldo-hidden-face))
      (overlay-put o 'display                 (propertize "\n<...>\n" 'face 'eldo-hidden-face))
      )
    ;; Prevent modifications inside hidden text
    (overlay-put o 'modification-hooks      '(eldo-overlay-prevent-modification)      )
    ;; Enable `isearch' support to reveal hidden text when searched inside
    (overlay-put o 'isearch-open-invisible  #'eldo-isearch-open-invisible             )
    ;; Enable `reveal-mode' support to reveal hidden text when `point' is around
    (overlay-put o 'reveal-toggle-invisible #'eldo-reveal-toggle-invisible            )
    (put 'eldo     'reveal-toggle-invisible #'eldo-reveal-toggle-invisible            )
    ))

(defun eldo-overlay-prevent-modification (_overlay _after _beg _end &optional _length)
  "Prevent modifications inside OVERLAY."
  (error "Cannot modify hidden text"))

(defun eldo-reveal-toggle-invisible (overlay hidep)
  "Delete OVERLAY when reveal mode is enabled"
  (save-excursion (delete-overlay overlay)))

(defun eldo-isearch-open-invisible (o)
  "Delete OVERLAY when searched inside"
  (delete-overlay o))

(defun eldo-hide-comments ()
  "Hide all comment sections in current buffer"
  (interactive)
  (save-excursion
    (dolist (regex '( "^\s*#c\\([^\n]\\|\n\\)*#e" ; block comments
                      "^\\(\s*\\*[^\n]+\n\\)+"    ; line comments (grouped when consecutive)
                     ;"^\\(\s*![^\n]+\n\\)+"      ; doc comments  (grouped when consecutive)
                      ))
      ;; Move to beginning of buffer
      ;; Search for all comments and hide them
      (goto-char (point-min))
      (while (re-search-forward regex nil t)
        (let ( ;; Do not include first char, so user knows what's hidden
               ( beg (progn (goto-char (match-beginning 0)) (forward-char 1)                (point)) )
               ;; Also do not include final newline
               ( end (progn (goto-char (match-end       0)) (when (bolp) (forward-char -1)) (point)) )
               )
          (eldo-hide-region beg end "eldo-comments")
          ))
      )
    ))

(defun eldo-fold-subckts ()
  "Hide all .subckt expressions in current buffer."
  (interactive)
  (save-excursion
    ;; Move to beginning of buffer
    (goto-char (point-min))
    ;; Search for all .subckt expressions and hide them
    (while (re-search-forward "\\.subckt" nil t)
      (let ( ( beg (progn (end-of-line) (point)) )
             ( end (and (re-search-forward "\\.ends")
                        (progn (beginning-of-line) (point))) )
             )
        (when end (eldo-hide-region beg end "eldo-functions"))
        ))
    ))

;; Show Functions
(defun eldo-show-comments ()
  "Reveal all hidden comments in current buffer."
  (interactive)
  (let ( ( beg (point-min) )
         ( end (point-max) )
         )
    (remove-overlays beg end 'label 'eldo-comments)
    ))

(defun eldo-show-subckts ()
  "Reveal all folded functions in current buffer."
  (interactive)
  (let ( ( beg (point-min) )
         ( end (point-max) )
         )
    (remove-overlays beg end 'label 'eldo-functions)
    ))

(defun eldo-show-all ()
  "Reveal all hidden text in current buffer."
  (interactive)
  (eldo-show-comments )
  (eldo-show-subckts)
  )


;;-----------------------------------------------------------------------------
;; Changelog and sections support
;;-----------------------------------------------------------------------------

(defconst eldo-section-regexp-start "^\\s-*!>\\s-*"
  "Eldo mode section header start regexp.")

(defun eldo-section-bar (&optional aligned)
  "Insert dashed bar from column zero to end of line.
If optional argument is provided, bar will be added from current column."
  (interactive)
  (if (not aligned) (beginning-of-line) )
  (insert "!")
  (insert-char ?- (- eldo-end-comment-column (current-column)))
  (insert "\n"))

(defun eldo-add-section (section &optional arg)
  "Add a section in buffer at (optional) point arg"
  (interactive "ssection:")
  (if arg
      (goto-char arg))
  (eldo-section-bar)
  (insert
   (concat "!>  " section " \n"))
  (eldo-section-bar))

(defun eldo-goto-section (section)
  "Move point to the beginning of the specified section;
If the section is not found, leave point at previous location."
  (interactive "ssection: ")
  (let ((pos (point)))
    (goto-char (point-min))
    (if (not (re-search-forward
          (concat eldo-section-regexp-start section "\\b") nil t))
    (progn (message "Couldn't find section %s" section)
           (goto-char pos)
           )
      (progn
    (forward-line 2)
    ;;(recenter)
    ))))

(defun eldo-add-changelog-entry (changelog-entry)
  "Find changelog section (create it if not found) and add an entry for today."
  (interactive "sChangelog entry: ")
  (goto-char (point-min))
  (if (not (re-search-forward (concat eldo-section-regexp-start "Changelog") nil t))
      (eldo-add-section "Changelog" (point-max) ))
  (eldo-goto-section "Changelog")
  (let ((string (concat "! " (substring (current-time-string) 0 11)
            (substring (current-time-string) -4) " "
            (user-full-name) " <" user-mail-address ">")))
    (if (not (search-forward string nil t))
    (insert "\n" string "\n")
      (forward-line 1))
    (insert "   - " changelog-entry "\n")))


;;-----------------------------------------------------------------------------
;; File initialization support
;;-----------------------------------------------------------------------------

(defun eldo-default-header ()
  "Insert default header"
  (eldo-section-bar)
  (insert
         "!  " (buffer-name) "\n"
         "!  Author(s): " user-mail-address "\n"
         "!  Copyright ©" (format-time-string "%Y") " STMicroelectronics\n"
  )
  (eldo-section-bar))

(defun eldo-file-initialize ()
  "Create a template for new file"
  (interactive)
  (eldo-default-header)
  (insert "\n\n\n\n")
  (eldo-add-section "Libraries")
  (insert "\n\n\n\n")
  (insert ".End")
  (insert "\n\n\n")
  (eldo-add-section "Changelog")
  (insert "\n\n\n\n*** Local Variables:\n*** mode:eldo\n*** End:\n")
  (eldo-add-changelog-entry "File created"))


;;-----------------------------------------------------------------------------
;; Templates/skeletons
;;-----------------------------------------------------------------------------

;; -------------------
;; Passive elements
;; -------------------

(tempo-define-template
 "eldo-resistor"
 '("R"
   (p "[name]: ") '(just-one-space)
   (p "[pos node]: ") '(just-one-space)
   (p "[neg node]: ") '(just-one-space)
   (p "<mname>: " mname) '(just-one-space)
   (if (string-equal (tempo-lookup-named 'mname) "")
       (list 'l "r=" '(p "[val]: "))
     (list 'l '(p "[val]: "))) '(just-one-space)
   (p "<temp coef 1>: " tc1 'noinsert)
   (if (string-equal (tempo-lookup-named 'tc1) "") ()
     (list 'l "tc1=" '(s tc1) '(p "<temp coef 2>: " tc2 'noinsert)))
   '(just-one-space)
   (if (and (tempo-lookup-named 'tc2)
        (not (string-equal (tempo-lookup-named 'tc2) "")))
       (list 'l "tc2=" '(s tc2)))
   '(just-one-space)
   (p "<ac resistance>: " ac 'noinsert)
   (if (string-equal (tempo-lookup-named 'ac) "")
       () (list 'l "ac=" '(s ac)))
   '(just-one-space)
   (p "<temp>: " temp 'noinsert)
   (if (string-equal (tempo-lookup-named 'temp) "")
       () (list 'l "t=" '(s temp)))
   '(just-one-space)
   (p "<m>: " m 'noinsert)
   (if (string-equal (tempo-lookup-named 'm) "")
       () (list 'l "m=" '(s m)))
   '(just-one-space)
   (p "<nonoise in transient [y/n]?>: " nonoise 'noinsert)
   (if (string-equal (tempo-lookup-named 'nonoise) "y")
       (list 'l "nonoise"))
   '(just-one-space)
   (p "<kf>: " kf 'noinsert)
   (if (string-equal (tempo-lookup-named 'kf) "")
       () (list 'l '(s kf) '(p "<af>: " af 'noinsert)))
   '(just-one-space)
   (if (and (tempo-lookup-named 'af)
        (not (string-equal (tempo-lookup-named 'af) "")))
       (list 'l '(s af)))
   '(just-one-space)
   )
  "r"
  "tempo template for eldo resistor"
  'eldo-tempo-tags)

(tempo-define-template
 "eldo-capacitor"
 '("C"
   (p "[name]: ") '(just-one-space)
   (p "[pos node]: ") '(just-one-space)
   (p "[neg node]: ") '(just-one-space)
   (p "<mname | poly>: " mname) '(just-one-space)
   (if (string-equal (tempo-lookup-named 'mname) "poly")
       (list 'l '(p "[val and poly coefficients]: "))
     (list 'l '(p "[val]: ")))
   '(just-one-space)
   (p "<m>: " m 'noinsert)
   (if (string-equal (tempo-lookup-named 'm) "")
       () (list 'l "m=" '(s m)))
   '(just-one-space)
   (p "<length>: " l 'noinsert)
   (if (string-equal (tempo-lookup-named 'l) "")
       () (list 'l "l=" '(s l)))
   '(just-one-space)
   (p "<width>: " w 'noinsert)
   (if (string-equal (tempo-lookup-named 'w) "")
       () (list 'l "w=" '(s w)))
   '(just-one-space)
   (p "<diff temp>: " dtemp 'noinsert)
   (if (string-equal (tempo-lookup-named 'dtemp) "")
       () (list 'l "dtemp=" '(s dtemp)))
   '(just-one-space)
   (p "<temp coef 1>: " tc1 'noinsert)
   (if (string-equal (tempo-lookup-named 'tc1) "") ()
     (list 'l "tc1=" '(s tc1) '(p "<temp coef 2>: " tc2 'noinsert)))
   '(just-one-space)
   (if (and (tempo-lookup-named 'tc2)
        (not (string-equal (tempo-lookup-named 'tc2) "")))
       (list 'l "tc2=" '(s tc2)))
   '(just-one-space)
   (p "<initial cond (voltage)>: " ic 'noinsert)
   (if (string-equal (tempo-lookup-named 'ic) "")
       () (list 'l "ic=" '(s ic)))
   )
  "C"
  "tempo template for eldo capacitor"
  'eldo-tempo-tags)

(tempo-define-template
  "eldo-inductor"
  '("L"
  (p "[name]: ") '(just-one-space)
  (p "[pos node]: ") '(just-one-space)
  (p "[neg node]: ") '(just-one-space)
  (p "<mname | poly>: " mname) '(just-one-space)
  (if (string-equal (tempo-lookup-named 'mname) "poly")
      (list 'l '(p "[val and poly coefficients]: "))
    (list 'l '(p "[val]: ")))
  '(just-one-space)
  (p "<initial cond (current)>: " ic 'noinsert)
  (if (string-equal (tempo-lookup-named 'ic) "")
      () (list 'l "ic=" '(s ic)))
  )
  "L"
  "tempo template for eldo inductor"
  'eldo-tempo-tags)

(tempo-define-template
 "eldo-rcline"
 '("R"
   (p "[name]: ") '(just-one-space)
   (p "[pos node]: ") '(just-one-space)
   (p "[neg node]: ") '(just-one-space)
   (p "[mname]: " mname) '(just-one-space)
   (p "<res>: " r 'noinsert)
   (if (string-equal (tempo-lookup-named 'r) "")
       () (list 'l "r=" '(s r)))
   '(just-one-space)
   (p "<temp coef 1>: " tc1 'noinsert)
   (if (string-equal (tempo-lookup-named 'tc1) "") ()
     (list 'l "tc1=" '(s tc1) '(p "<temp coef 2>: " tc2 'noinsert)))
   '(just-one-space)
   (if (and (tempo-lookup-named 'tc2)
        (not (string-equal (tempo-lookup-named 'tc2) "")))
       (list 'l "tc2=" '(s tc2)))
   '(just-one-space)
   (p "<cap>: " c 'noinsert)
   (if (string-equal (tempo-lookup-named 'c) "")
       () (list 'l "c=" '(s c)))
   '(just-one-space)
   (p "<length>: " l 'noinsert)
   (if (string-equal (tempo-lookup-named 'l) "")
       () (list 'l "l=" '(s l)))
   '(just-one-space)
   (p "<width>: " w 'noinsert)
   (if (string-equal (tempo-lookup-named 'w) "")
       () (list 'l "w=" '(s w)))
   '(just-one-space)
   (p "<m>: " m 'noinsert)
   (if (string-equal (tempo-lookup-named 'm) "")
       () (list 'l "m=" '(s m)))
   '(just-one-space)
   (p "<diff temp>: " dtemp 'noinsert)
   (if (string-equal (tempo-lookup-named 'dtemp) "")
       () (list 'l "dtemp=" '(s dtemp)))
   '(just-one-space)
   (p "<scale>: " scale 'noinsert)
   (if (string-equal (tempo-lookup-named 'scale) "")
       () (list 'l "scale=" '(s scale)))
   '(just-one-space)
   )
  "RC"
  "tempo template for eldo rcline"
  'eldo-tempo-tags)

;; -------------------
;; Active elements
;; -------------------

(tempo-define-template
  "eldo-diode"
  '("D"
   (p "[name]: ") '(just-one-space)
   (p "[positive node]: ") '(just-one-space)
   (p "[negative node]: ") '(just-one-space)
   (p "[mname]: ") '(just-one-space)
   (p "<area>: " area 'noinsert) '(just-one-space)
   (if (string-equal (tempo-lookup-named 'area) "")
       () (list 'l "area=" '(s area)))
   '(just-one-space)
   (p "<perimeter>: " peri 'noinsert)
   (if (string-equal (tempo-lookup-named 'peri) "")
       () (list 'l "peri=" '(s peri)))
   '(just-one-space)
   (p "<temp>: " temp 'noinsert)
   (if (string-equal (tempo-lookup-named 'temp) "")
       () (list 'l "temp=" '(s temp)))
   '(just-one-space)
   (p "<off [y/n]>: " off 'noinsert)
   (if (and (tempo-lookup-named 'off)
        (string-equal (tempo-lookup-named 'off) "y"))
       (list 'l "off"))
   '(just-one-space)
   (p "<nonoise [y/n]>: " nonoise 'noinsert)
   (if (and (tempo-lookup-named 'nonoise)
        (string-equal (tempo-lookup-named 'nonoise) "y"))
       (list 'l "nonoise"))
   '(just-one-space)
   )
  "D"
  "tempo template for eldo diode"
  'eldo-tempo-tags)

(tempo-define-template
 "eldo-bipolar"
 '("Q"
   (p "[name]: ") '(just-one-space)
   (p "[collector node]: ") '(just-one-space)
   (p "[base node]: ") '(just-one-space)
   (p "[emitter node]: ") '(just-one-space)
   (p "<substrate node>: ") '(just-one-space)
   (p "[mname]: ") '(just-one-space)
   (p "<area>: " area 'noinsert) '(just-one-space)
   (if (string-equal (tempo-lookup-named 'area) "")
       () (list 'l "area=" '(s area)))
   '(just-one-space)
   (p "<rel base area>: " areab 'noinsert)
   (if (string-equal (tempo-lookup-named 'areab) "")
       () (list 'l "areab=" '(s areab)))
   '(just-one-space)
   (p "<rel collector area>: " areac 'noinsert)
   (if (string-equal (tempo-lookup-named 'areac) "")
       () (list 'l "areac=" '(s areac)))
   '(just-one-space)
   (p "<temp>: " temp 'noinsert)
   (if (string-equal (tempo-lookup-named 'temp) "")
       () (list 'l "t=" '(s temp)))
   '(just-one-space)
   (p "<m>: " m 'noinsert)
   (if (string-equal (tempo-lookup-named 'm) "")
       () (list 'l "m=" '(s m)))
   '(just-one-space)
   (p "<off [y/n]>: " off 'noinsert) '(just-one-space)
   (if (and (tempo-lookup-named 'off)
        (string-equal (tempo-lookup-named 'off) "y"))
       (list 'l "off"))
   '(just-one-space)
   (p "<nonoise [y/n]>: " nonoise 'noinsert)
   (if (and (tempo-lookup-named 'nonoise)
        (string-equal (tempo-lookup-named 'nonoise) "y"))
       (list 'l "nonoise"))
   '(just-one-space)
   )
 "Q"
 "tempo template for eldo bipolar"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-jfet"
 '("J"
   (p "[name]: ") '(just-one-space)
   (p "[drain node]: ") '(just-one-space)
   (p "[gate node]: ") '(just-one-space)
   (p "[source node]: ") '(just-one-space)
   (p "[mname]: ") '(just-one-space)
   (p "<area>: " area 'noinsert) '(just-one-space)
   (if (string-equal (tempo-lookup-named 'area) "")
       () (list 'l "area=" '(s area)))
   '(just-one-space)
   (p "<length>: " l 'noinsert)
   (if (string-equal (tempo-lookup-named 'l) "")
       () (list 'l "l=" '(s l)))
   '(just-one-space)
   (p "<width>: " w 'noinsert)
   (if (string-equal (tempo-lookup-named 'w) "")
       () (list 'l "w=" '(s w)))
   '(just-one-space)
   (p "<temp>: " temp 'noinsert)
   (if (string-equal (tempo-lookup-named 'temp) "")
       () (list 'l "t=" '(s temp)))
   '(just-one-space)
   (p "<off [y/n]>: " off 'noinsert) '(just-one-space)
   (if (and (tempo-lookup-named 'off)
        (string-equal (tempo-lookup-named 'off) "y"))
       (list 'l "off"))
   '(just-one-space)
   (p "<nonoise [y/n]>: " nonoise 'noinsert)
   (if (and (tempo-lookup-named 'nonoise)
        (string-equal (tempo-lookup-named 'nonoise) "y"))
       (list 'l "nonoise"))
   '(just-one-space)
   )
 "J"
 "tempo template for eldo jfet"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-mosfet"
 '("M"
   (p "[name]: ") '(just-one-space)
   (p "[drain node]: ") '(just-one-space)
   (p "[gate node]: ") '(just-one-space)
   (p "[source node]: ") '(just-one-space)
   (p "[bulk node]: ") '(just-one-space)
   (p "<optional nodes>: ") '(just-one-space)
   "mod="
   (p "[mname]: ") '(just-one-space)
   (p "<length>: " l 'noinsert)
   (if (string-equal (tempo-lookup-named 'l) "")
       () (list 'l "l=" '(s l)))
   '(just-one-space)
   (p "<width>: " w 'noinsert)
   (if (string-equal (tempo-lookup-named 'w) "")
       () (list 'l "w=" '(s w)))
   '(just-one-space)
   (p "<area drain>: " ad 'noinsert)
   (if (string-equal (tempo-lookup-named 'ad) "")
       () (list 'l "ad=" '(s ad)))
   '(just-one-space)
   (p "<area source>: " as 'noinsert)
   (if (string-equal (tempo-lookup-named 'as) "")
       () (list 'l "as=" '(s as)))
   '(just-one-space)
   (p "<perimeter drain>: " pd 'noinsert)
   (if (string-equal (tempo-lookup-named 'pd) "")
       () (list 'l "pd=" '(s pd)))
   '(just-one-space)
   (p "<perimeter source>: " ps 'noinsert)
   (if (string-equal (tempo-lookup-named 'ps) "")
       () (list 'l "ps=" '(s ps)))
   '(just-one-space)
   (p "<geometry model>: " geo 'noinsert)
   (if (string-equal (tempo-lookup-named 'geo) "")
       () (list 'l "geo=" '(s geo)))
   '(just-one-space)
   (p "<number of drain contacts>: " nrd 'noinsert)
   (if (string-equal (tempo-lookup-named 'nrd) "")
       () (list 'l "nrd=" '(s nrd)))
   '(just-one-space)
   (p "<number of source contacts>: " nrs 'noinsert)
   (if (string-equal (tempo-lookup-named 'nrs) "")
       () (list 'l "nrs=" '(s nrs)))
   '(just-one-space)
   (p "<m>: " m 'noinsert)
   (if (string-equal (tempo-lookup-named 'm) "")
       () (list 'l "m=" '(s m)))
   '(just-one-space)
   (p "<extra drain contact resistance>: " rdc 'noinsert)
   (if (string-equal (tempo-lookup-named 'rdc) "")
       () (list 'l "rdc=" '(s rdc)))
   '(just-one-space)
   (p "<extra source contact resistance>: " rsc 'noinsert)
   (if (string-equal (tempo-lookup-named 'rsc) "")
       () (list 'l "rsc=" '(s rsc)))
   '(just-one-space)
   (p "<temp>: " temp 'noinsert)
   (if (string-equal (tempo-lookup-named 'temp) "")
       () (list 'l "t=" '(s temp)))
   '(just-one-space)
   (p "<off [y/n]>: " off 'noinsert) '(just-one-space)
   (if (and (tempo-lookup-named 'off)
        (string-equal (tempo-lookup-named 'off) "y"))
       (list 'l "off"))
   '(just-one-space)
   (p "<nonoise [y/n]>: " nonoise 'noinsert)
   (if (and (tempo-lookup-named 'nonoise)
        (string-equal (tempo-lookup-named 'nonoise) "y"))
       (list 'l "nonoise"))
   '(just-one-space)
   )
 "M"
 "tempo template for eldo mosfet"
 'eldo-tempo-tags)

;; -------------------
;; Voltage sources
;; -------------------

(tempo-define-template
 "eldo-vcvs"
 '("E"
   (p "[name]: ") '(just-one-space)
   (p "[positive node]: ") '(just-one-space)
   (p "[negative node]: ") '(just-one-space)
   (p "[positive controlling node]: ") '(just-one-space)
   (p "[negative controlling node]: ") '(just-one-space)
   (p "[gain]: ") '(just-one-space)
   )
 "vcvs"
 "template for inserting an ELDO voltage controlled voltage source"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-ccvs"
 '("H"
   (p "[name]: ") '(just-one-space)
   (p "[positive node]: ") '(just-one-space)
   (p "[negative node]: ") '(just-one-space)
   (p "[current source]: ") '(just-one-space)
   (p "[gain]: ") '(just-one-space)
   )
 "ccvs"
 "template for inserting an ELDO current controlled voltage source"
 'eldo-tempo-tags)

;; -------------------
;; Current sources
;; -------------------

(tempo-define-template
 "eldo-vccs"
 '("G"
   (p "[name]: ") '(just-one-space)
   (p "[positive node]: ") '(just-one-space)
   (p "[negative node]: ") '(just-one-space)
   (p "[positive controlling node]: ") '(just-one-space)
   (p "[negative controlling node]: ") '(just-one-space)
   (p "[transadmitance]: ") '(just-one-space)
   )
 "vccs"
 "template for inserting an ELDO voltage controlled current source"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-cccs"
 '("F"
   (p "[name]: ") '(just-one-space)
   (p "[positive node]: ") '(just-one-space)
   (p "[negative node]: ") '(just-one-space)
   (p "[current source]: ") '(just-one-space)
   (p "[gain]: ") '(just-one-space)
   )
 "cccs"
 "template for inserting an ELDO current controlled current source"
 'eldo-tempo-tags)


;; -------------------
;; Waveforms
;; -------------------

(define-skeleton eldo-pwl
  "Skeleton for Piece Wise Linear waveform"
  "time/value doublet: "
  "pwl(" str
  ( "next doublet: (%s) "
    " "str )
  resume:
  ")")

(tempo-define-template
 "eldo-pulse"
 '("pulse("
   (p "[start value]: ") '(just-one-space)
   (p "[pulsed value]: ") '(just-one-space)
   (p "[delay]: ") '(just-one-space)
   (p "[rise time]: ") '(just-one-space)
   (p "[fall time]: ") '(just-one-space)
   (p "[pulse duration]: ") '(just-one-space)
   (p "[period]: ")
   ")")
 "pulse"
 "template for inserting an ELDO Pulse waveform"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-ac"
 '("ac("
   (p "[magnitude]: ") '(just-one-space)
   (p "[phase]: ") '(just-one-space)
   ")")
 "ac"
 "template for inserting an ELDO AC waveform"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-am"
 '("am("
   (p "[amplitude]: ") '(just-one-space)
   (p "[offset]: ") '(just-one-space)
   (p "[modulation frequency]: ") '(just-one-space)
   (p "[carrier frequency]: ") '(just-one-space)
   (p "[delay]: ") '(just-one-space)
   ")")
 "am"
 "template for inserting an ELDO amplitude modulation signal"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-pattern"
 '("pattern "
   (p "[Vhi]: ") '(just-one-space)
   (p "[Vlo]: ") '(just-one-space)
   (p "[delay]: ") '(just-one-space)
   (p "[rise time]: ") '(just-one-space)
   (p "[fall time]: ") '(just-one-space)
   (p "[Bit duration]: ") '(just-one-space)
   (p "[Bits]: ")
   )
 "pattern"
 "template for inserting an ELDO Pattern function"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-sine"
 '("sin("
   (p "[offset]: ") '(just-one-space)
   (p "[amplitude]: ") '(just-one-space)
   (p "[frequency]: ") '(just-one-space)
   (p "[delay]: ") '(just-one-space)
   (p "[damping factor]: ") '(just-one-space)
   (p "[phase delay]: ") '(just-one-space)
   ")")
 "sin"
 "template for inserting an ELDO SINE function"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-sffm"
 '("sffm("
   (p "[offset]: ") '(just-one-space)
   (p "[amplitude]: ") '(just-one-space)
   (p "[carrier frequency]: ") '(just-one-space)
   (p "[modulation index]: ") '(just-one-space)
   (p "[signal frequency]: ") '(just-one-space)
   ")")
 "sffm"
 "template for inserting an ELDO Single Frequency FM function"
 'eldo-tempo-tags
)

(tempo-define-template
 "eldo-exp"
 '("exp("
   (p "[start value]: ") '(just-one-space)
   (p "[target value]: ") '(just-one-space)
   (p "[rise delay]: ") '(just-one-space)
   (p "[tau1]: ") '(just-one-space)
   (p "[fall delay]: ") '(just-one-space)
   (p "[tau2]: ") '(just-one-space)
   ")")
 "exp"
 "template for inserting an ELDO EXP waveform"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-noise"
 '("noise "
   (p "[White noise level]: ") '(just-one-space)
   (p "[Flicker noise level]: ") '(just-one-space)
   (p "[Alpha]: ") '(just-one-space)
   (p "[Cut-off freq]: ") '(just-one-space)
   (p "[Filter order]: ") '(just-one-space)
   " ")
 "noise"
 "template for inserting an ELDO NOISE waveform"
 'eldo-tempo-tags)

;; -------------------
;; Analog Macromodels
;; -------------------

(tempo-define-template
 "eldo-comp"
 '("comp"
   (p "[instance name]: ") " "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " "
   (p "[output]: ") " "
   (p "[model name]: ") " "
   (p "<vhigh>: " vhi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vhi) "")
       (list 'l "vhi=5.0")  ;; default value
     (list 'l "vhi=" '(s vhi)))
   '(just-one-space)
   (p "<vlow>: " vlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vlo) "")
       (list 'l "vlo=0.0")  ;; default value
     (list 'l "vlo=" '(s vlo)))
   '(just-one-space)
   (p "<input offset>: " voff 'noinsert)
   (if (string-equal (tempo-lookup-named 'voff) "")
       (list 'l "voff=0.0") ;; default value
     (list 'l "voff=" '(s voff)))
   '(just-one-space)
   (p "<hysteresis voltage>: " vdef 'noinsert)
   (if (string-equal (tempo-lookup-named 'vdef) "")
       (list 'l "vdef=0.0") ;; default value
     (list 'l "vdef=" '(s vdef)))
   '(just-one-space)
   (p "<commutation time>: " tcom 'noinsert)
   (if (string-equal (tempo-lookup-named 'tcom) "")
       (list 'l "tcom=1ns") ;; default value
     (list 'l "tcom=" '(s tcom)))
   '(just-one-space)
   (p "<transit time>: " tpd 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpd) "")
       (list 'l "tpd=0.0")  ;; default value
     (list 'l "tpd=" '(s tpd)))
   '(just-one-space)
   'n)
 "comp"
 "template for inserting an ELDO Single output comparator"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-compd"
 '("compd"
   (p "[instance name]: ") " "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " "
   (p "[positive output]: ") " "
   (p "[negative output]: ") " "
   (p "[model name]: ") " "
   (p "<vhigh>: " vhi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vhi) "")
       (list 'l "vhi=5.0")  ;; default value
     (list 'l "vhi=" '(s vhi)))
   '(just-one-space)
   (p "<vlow>: " vlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vlo) "")
       (list 'l "vlo=0.0")  ;; default value
     (list 'l "vlo=" '(s vlo)))
   '(just-one-space)
   (p "<input offset>: " voff 'noinsert)
   (if (string-equal (tempo-lookup-named 'voff) "")
       (list 'l "voff=0.0") ;; default value
     (list 'l "voff=" '(s voff)))
   '(just-one-space)
   (p "<hysteresis voltage>: " vdef 'noinsert)
   (if (string-equal (tempo-lookup-named 'vdef) "")
       (list 'l "vdef=0.0") ;; default value
     (list 'l "vdef=" '(s vdef)))
   '(just-one-space)
   (p "<commutation time>: " tcom 'noinsert)
   (if (string-equal (tempo-lookup-named 'tcom) "")
       (list 'l "tcom=1ns") ;; default value
     (list 'l "tcom=" '(s tcom)))
   '(just-one-space)
   (p "<transit time>: " tpd 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpd) "")
       (list 'l "tpd=0.0")  ;; default value
     (list 'l "tpd=" '(s tpd)))
   '(just-one-space)
   'n)
 "compd"
 "template for inserting an ELDO Differential output comparator"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-linear-opa0"
 '("Y"
   (p "[instance name]: ") " opamp0 "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " "
   (p "[output]: ") " "
   (p "[ground]: ") " param: "
   (p "<gain>: " gain 'noinsert)
   (if (string-equal (tempo-lookup-named 'gain) "")
       (list 'l "gain=1e5") ;; default value
     (list 'l "gain=" '(s gain)))
   '(just-one-space)
   (p "<input impedance>: " rin 'noinsert)
   (if (string-equal (tempo-lookup-named 'rin) "")
       (list 'l "rin=1e7")  ;; default value
     (list 'l "rin=" '(s rin)))
   '(just-one-space)
   'n)
 "opa0"
 "tempo template for eldo single output linear opamp"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-linear-opa0d"
 '("Y"
   (p "[instance name]: ") " opamp0d "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " "
   (p "[positive output]: ") " "
   (p "[negative output]: ") " "
   (p "[ground]: ") " param: "
   (p "<gain>: " gain 'noinsert)
   (if (string-equal (tempo-lookup-named 'gain) "")
       (list 'l "gain=1e5") ;; default value
     (list 'l "gain=" '(s gain)))
   '(just-one-space)
   (p "<input impedance>: " rin 'noinsert)
   (if (string-equal (tempo-lookup-named 'rin) "")
       (list 'l "rin=1e7")  ;; default value
     (list 'l "rin=" '(s rin)))
   '(just-one-space)
   'n)
 "opa0d"
 "template for inserting an ELDO differential output linear opamp"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-linear-opa1"
 '("Y"
   (p "[instance name]: ") " opamp1 "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " "
   (p "[output]: ") " "
   (p "[ground]: ") " param: "
   (p "<gain>: " gain 'noinsert)
   (if (string-equal (tempo-lookup-named 'gain) "")
       (list 'l "gain=1e5") ;; default value
     (list 'l "gain=" '(s gain)))
   '(just-one-space)
   (p "<input offset>: " voff 'noinsert)
   (if (string-equal (tempo-lookup-named 'voff) "")
       (list 'l "voff=0.0") ;; default value
     (list 'l "voff=" '(s voff)))
   '(just-one-space)
   (p "<dominant pole>: " p1 'noinsert)
   (if (string-equal (tempo-lookup-named 'p1) "")
       (list 'l "p1=100")   ;; default value
     (list 'l "p1=" '(s p1)))
   '(just-one-space)
   (p "<input impedance>: " rin 'noinsert)
   (if (string-equal (tempo-lookup-named 'rin) "")
       (list 'l "rin=1e7")  ;; default value
     (list 'l "rin=" '(s rin)))
   '(just-one-space)
   'n)
 "opa1"
 "template for inserting an ELDO single output 1-pole linear opamp"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-linear-opa1d"
 '("Y"
   (p "[instance name]: ") " opamp1d "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " "
   (p "[positive output]: ") " "
   (p "[negative output]: ") " "
   (p "[ground]: ") " param: "
   (p "<gain>: " gain 'noinsert)
   (if (string-equal (tempo-lookup-named 'gain) "")
       (list 'l "gain=1e5") ;; default value
     (list 'l "gain=" '(s gain)))
   '(just-one-space)
   (p "<input offset>: " voff 'noinsert)
   (if (string-equal (tempo-lookup-named 'voff) "")
       (list 'l "voff=0.0") ;; default value
     (list 'l "voff=" '(s voff)))
   '(just-one-space)
   (p "<dominant pole>: " p1 'noinsert)
   (if (string-equal (tempo-lookup-named 'p1) "")
       (list 'l "p1=100")   ;; default value
     (list 'l "p1=" '(s p1)))
   '(just-one-space)
   (p "<input impedance>: " rin 'noinsert)
   (if (string-equal (tempo-lookup-named 'rin) "")
       (list 'l "rin=1e7")  ;; default value
     (list 'l "rin=" '(s rin)))
   '(just-one-space)
   (p "<common mode rejection ratio>: " cmrr 'noinsert)
   (if (string-equal (tempo-lookup-named 'cmrr) "")
       (list 'l "cmrr=0.0") ;; default value
     (list 'l "cmrr=" '(s cmrr)))
   '(just-one-space)
   'n)
 "opa1d"
 "template for inserting an ELDO differential output 1-pole linear opamp"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-linear-opa2"
 '("Y"
   (p "[instance name]: ") " opamp2 "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " "
   (p "[output]: ") " "
   (p "[ground]: ") " param: "
   (p "<gain>: " gain 'noinsert)
   (if (string-equal (tempo-lookup-named 'gain) "")
       (list 'l "gain=1e5") ;; default value
     (list 'l "gain=" '(s gain)))
   '(just-one-space)
   (p "<input offset>: " voff 'noinsert)
   (if (string-equal (tempo-lookup-named 'voff) "")
       (list 'l "voff=0.0") ;; default value
     (list 'l "voff=" '(s voff)))
   '(just-one-space)
   (p "<dominant pole>: " p1 'noinsert)
   (if (string-equal (tempo-lookup-named 'p1) "")
       (list 'l "p1=100")   ;; default value
     (list 'l "p1=" '(s p1)))
   '(just-one-space)
   (p "<non-dominant pole>: " p2 'noinsert)
   (if (string-equal (tempo-lookup-named 'p2) "")
       (list 'l "p2=1e6")   ;; default value
     (list 'l "p2=" '(s p2)))
   '(just-one-space)
   (p "<input impedance>: " rin 'noinsert)
   (if (string-equal (tempo-lookup-named 'rin) "")
       (list 'l "rin=1e7")  ;; default value
     (list 'l "rin=" '(s rin)))
   '(just-one-space)
   'n)
 "opa2"
 "template for inserting an ELDO single output 2-pole linear opamp"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-linear-opa2d"
 '("Y"
   (p "[instance name]: ") " opamp2d "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " "
   (p "[positive output]: ") " "
   (p "[negative output]: ") " "
   (p "[ground]: ") " param: "
   (p "<gain>: " gain 'noinsert)
   (if (string-equal (tempo-lookup-named 'gain) "")
       (list 'l "gain=1e5") ;; default value
     (list 'l "gain=" '(s gain)))
   '(just-one-space)
   (p "<input offset>: " voff 'noinsert)
   (if (string-equal (tempo-lookup-named 'voff) "")
       (list 'l "voff=0.0") ;; default value
     (list 'l "voff=" '(s voff)))
   '(just-one-space)
   (p "<dominant pole>: " p1 'noinsert)
   (if (string-equal (tempo-lookup-named 'p1) "")
       (list 'l "p1=100")   ;; default value
     (list 'l "p1=" '(s p1)))
   '(just-one-space)
   (p "<non-dominant pole>: " p2 'noinsert)
   (if (string-equal (tempo-lookup-named 'p2) "")
       (list 'l "p2=1e6")   ;; default value
     (list 'l "p2=" '(s p2)))
   '(just-one-space)
   (p "<input impedance>: " rin 'noinsert)
   (if (string-equal (tempo-lookup-named 'rin) "")
       (list 'l "rin=1e7")  ;; default value
     (list 'l "rin=" '(s rin)))
   '(just-one-space)
   (p "<common mode rejection ratio>: " cmrr 'noinsert)
   (if (string-equal (tempo-lookup-named 'cmrr) "")
       (list 'l "cmrr=0.0") ;; default value
     (list 'l "cmrr=" '(s cmrr)))
   '(just-one-space)
   'n)
 "opa2d"
 "template for inserting an ELDO differential output 2-pole linear opamp"
 'eldo-tempo-tags)


(tempo-define-template
 "eldo-delay"
 '("del"
   (p "[instance name]: ") " "
   (p "[input]: ") " "
   (p "[output]: ") " "
   (p "[delay value]: ") " "
   'n)
 "del"
 "template for inserting an ELDO delay"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-satr"
 '("Y"
   (p "[instance name]: ") " satr "
   (p "[input]: ") " "
   (p "[output]: ") " param: "
   (p "<value of resistance>: " r 'noinsert)
   (if (string-equal (tempo-lookup-named 'r) "")
       (list 'l "r=1")  ;; default value
     (list 'l "r=" '(s r)))
   '(just-one-space)
   (p "<max current>: " imax 'noinsert)
   (if (string-equal (tempo-lookup-named 'imax) "")
       (list 'l "imax=1")   ;; default value
     (list 'l "imax=" '(s imax)))
   '(just-one-space)
   (p "<slew rate(v/µs)>: " sr 'noinsert)
   (if (string-equal (tempo-lookup-named 'sr) "")
       (list 'l "sr=0") ;; default value
     (list 'l "sr=" '(s sr)))
   '(just-one-space)
   (p "<dominant pole>: " p1 'noinsert)
   (if (string-equal (tempo-lookup-named 'p1) "")
       (list 'l "p1=1e6")   ;; default value
     (list 'l "p1=" '(s p1)))
   '(just-one-space)
   (p "<resistance of low-pass filter>: " r1 'noinsert)
   (if (string-equal (tempo-lookup-named 'r1) "")
       (list 'l "r1=30")    ;; default value
     (list 'l "r1=" '(s r1)))
   'n)
 "satr"
 "template for inserting an ELDO saturating resistor"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-satv"
 '("Y"
   (p "[instance name]: ") " satv "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " "
   (p "[positive output]: ") " "
   (p "[negative output]: ") " param: "
   (p "<vmax>: " vmax 'noinsert)
   (if (string-equal (tempo-lookup-named 'vmax) "")
       (list 'l "vmax=5.0") ;; default value
     (list 'l "vmax=" '(s vmax)))
   '(just-one-space)
   (p "<vmin>: " vmin 'noinsert)
   (if (string-equal (tempo-lookup-named 'vmin) "")
       (list 'l "vmin=-5.0")    ;; default value
     (list 'l "vmin=" '(s vmin)))
   '(just-one-space)
   (p "<positive saturation voltage>: " vsatp 'noinsert)
   (if (string-equal (tempo-lookup-named 'vsatp) "")
       (list 'l "vsatp=4.75")   ;; default value
     (list 'l "vsatp=" '(s vsatp)))
   '(just-one-space)
   (p "<negative saturation voltage>: " vsatn 'noinsert)
   (if (string-equal (tempo-lookup-named 'vsatn) "")
       (list 'l "vsatn=-4.75")  ;; default value
     (list 'l "vsatn=" '(s vsatn)))
   '(just-one-space)
   (p "<slope at vsatp>: " pslope 'noinsert)
   (if (string-equal (tempo-lookup-named 'pslope) "")
       (list 'l "pslope=0.25")  ;; default value
     (list 'l "pslope=" '(s pslope)))
   '(just-one-space)
   (p "<slope at vsatn>: " nslope 'noinsert)
   (if (string-equal (tempo-lookup-named 'nslope) "")
       (list 'l "nslope=0.25")  ;; default value
     (list 'l "nslope=" '(s nslope)))
   'n)
 "satv"
 "template for inserting an ELDO voltage limiter"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-vswitch"
 '("Y"
   (p "[instance name]: ") " vswitch "
   (p "[input]: ") " "
   (p "[output]: ") " "
   (p "[positive controlling node]: ") " "
   (p "[negative controlling node]: ") " param: "
   (p "<level (1/2)>: " level 'noinsert)
   (if (string-equal (tempo-lookup-named 'level) "")
       (list 'l "level=1")  ;; default value
     (list 'l "level=2" ))
   '(just-one-space)
   (p "<voltage for 'on' state>: " von 'noinsert)
   (if (string-equal (tempo-lookup-named 'von) "")
       (list 'l "von=0.95") ;; default value
     (list 'l "von=" '(s von)))
   '(just-one-space)
   (p "<voltage for 'off' state>: " voff 'noinsert)
   (if (string-equal (tempo-lookup-named 'voff) "")
       (list 'l "voff=0.05")    ;; default value
     (list 'l "voff=" '(s voff)))
   '(just-one-space)
   (p "<ron resistance>: " ron 'noinsert)
   (if (string-equal (tempo-lookup-named 'ron) "")
       (list 'l "ron=1e-2") ;; default value
     (list 'l "ron=" '(s ron)))
   '(just-one-space)
   (p "<roff resistance>: " roff 'noinsert)
   (if (string-equal (tempo-lookup-named 'roff) "")
       (list 'l "roff=1e10")    ;; default value
     (list 'l "roff=" '(s roff)))
   'n)
 "vswitch"
 "template for inserting an ELDO voltage controlled switch"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-cswitch"
 '("Y"
   (p "[instance name]: ") " cswitch "
   (p "[input]: ") " "
   (p "[output]: ") " ic: "
   (p "[controlling current]: ") " param: "
   (p "<level (1/2)>: " level 'noinsert)
   (if (string-equal (tempo-lookup-named 'level) "")
       (list 'l "level=1")  ;; default value
     (list 'l "level=2" ))
   '(just-one-space)
   (p "<current for 'on' state>: " ion 'noinsert)
   (if (string-equal (tempo-lookup-named 'ion) "")
       (list 'l "ion=0.95") ;; default value
     (list 'l "ion=" '(s ion)))
   '(just-one-space)
   (p "<current for 'off' state>: " ioff 'noinsert)
   (if (string-equal (tempo-lookup-named 'ioff) "")
       (list 'l "ioff=0.05")    ;; default value
     (list 'l "ioff=" '(s ioff)))
   '(just-one-space)
   (p "<ron resistance>: " ron 'noinsert)
   (if (string-equal (tempo-lookup-named 'ron) "")
       (list 'l "ron=1e-2") ;; default value
     (list 'l "ron=" '(s ron)))
   '(just-one-space)
   (p "<roff resistance>: " roff 'noinsert)
   (if (string-equal (tempo-lookup-named 'roff) "")
       (list 'l "roff=1e10")    ;; default value
     (list 'l "roff=" '(s roff)))
   'n)
 "cswitch"
 "template for inserting an ELDO current controlled switch"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-tri2sin"
 '("Y"
   (p "[instance name]: ") " tri2sin "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " "
   (p "[positive output]: ") " "
   (p "[negative output]: ") " param: "
   (p "<level (1/2)>: " level 'noinsert)
   (if (string-equal (tempo-lookup-named 'level) "")
       (list 'l "level=1")  ;; default value
     (list 'l "level=" '(s level)))
   '(just-one-space)
   (p "<gain>: " gain 'noinsert)
   (if (string-equal (tempo-lookup-named 'gain) "")
       (list 'l "gain=1e5") ;; default value
     (list 'l "gain=" '(s gain)))
   '(just-one-space)
   (p "<input offset>: " voff 'noinsert)
   (if (string-equal (tempo-lookup-named 'voff) "")
       (list 'l "voff=0.0") ;; default value
     (list 'l "voff=" '(s voff)))
   '(just-one-space)
   (p "<upper input voltage limit >: " vu 'noinsert)
   (if (string-equal (tempo-lookup-named 'vu) "")
       (list 'l "vu=1")     ;; default value
     (list 'l "vu=" '(s vu)))
   '(just-one-space)
   (p "<lower input voltage limit >: " vl 'noinsert)
   (if (string-equal (tempo-lookup-named 'vl) "")
       (list 'l "vl=1")     ;; default value
     (list 'l "vl=" '(s vl)))
   'n)
 "tri2sin"
 "template for inserting an ELDO triangular to sine wave converter"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-stairgen"
 '("Y"
   (p "[instance name]: ") " stairgen "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " param: "
   (p "<start voltage>: " vstart 'noinsert)
   (if (string-equal (tempo-lookup-named 'vstart) "")
       (list 'l "vstart=0.0")   ;; default value
     (list 'l "vstart=" '(s vstart)))
   '(just-one-space)
   (p "<step voltage>: " vdelta 'noinsert)
   (if (string-equal (tempo-lookup-named 'vdelta) "")
       (list 'l "vdelta=0.1")   ;; default value
     (list 'l "vdelta=" '(s vdelta)))
   '(just-one-space)
   (p "<number of steps>: " nstep 'noinsert)
   (if (string-equal (tempo-lookup-named 'nstep) "")
       (list 'l "nstep=10") ;; default value
     (list 'l "nstep=" '(s nstep)))
   '(just-one-space)
   (p "<period>: " tdu 'noinsert)
   (if (string-equal (tempo-lookup-named 'tdu) "")
       (list 'l "tdu=1e-4") ;; default value
     (list 'l "tdu=" '(s tdu)))
   '(just-one-space)
   (p "<slew rate (v/µs)>: " slr 'noinsert)
   (if (string-equal (tempo-lookup-named 'slr) "")
       (list 'l "slr=1")    ;; default value
     (list 'l "slr=" '(s slr)))
   'n)
 "stairgen"
 "template for inserting an ELDO staircase waveform generator"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-sawgen"
 '("Y"
   (p "[instance name]: ") " sawgen "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " param: "
   (p "<start voltage>: " v0 'noinsert)
   (if (string-equal (tempo-lookup-named 'v0) "")
       (list 'l "v0=0.0")   ;; default value
     (list 'l "v0=" '(s v0)))
   '(just-one-space)
   (p "<voltage magnitude>: " v1 'noinsert)
   (if (string-equal (tempo-lookup-named 'v1) "")
       (list 'l "v1=5.0")   ;; default value
     (list 'l "v1=" '(s v1)))
   '(just-one-space)
   (p "<period>: " tdu 'noinsert)
   (if (string-equal (tempo-lookup-named 'tdu) "")
       (list 'l "tdu=1e-4") ;; default value
     (list 'l "tdu=" '(s tdu)))
   '(just-one-space)
   (p "<delay>: " tdel 'noinsert)
   (if (string-equal (tempo-lookup-named 'tdel) "")
       (list 'l "tdel=0.0") ;; default value
     (list 'l "tdel=" '(s tdel)))
   'n)
 "sawgen"
 "template for inserting an ELDO sawtooth waveform generator"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-trigen"
 '("Y"
   (p "[instance name]: ") " trigen "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " param: "
   (p "<start voltage>: " v0 'noinsert)
   (if (string-equal (tempo-lookup-named 'v0) "")
       (list 'l "v0=0.0")   ;; default value
     (list 'l "v0=" '(s v0)))
   '(just-one-space)
   (p "<voltage magnitude>: " v1 'noinsert)
   (if (string-equal (tempo-lookup-named 'v1) "")
       (list 'l "v1=5.0")   ;; default value
     (list 'l "v1=" '(s v1)))
   '(just-one-space)
   (p "<first edge duration>: " rdu 'noinsert)
   (if (string-equal (tempo-lookup-named 'rdu) "")
       (list 'l "rdu=1e-4") ;; default value
     (list 'l "rdu=" '(s rdu)))
   '(just-one-space)
   (p "<second edge duration>: " fdu 'noinsert)
   (if (string-equal (tempo-lookup-named 'fdu) "")
       (list 'l "fdu=1e-4") ;; default value
     (list 'l "fdu=" '(s fdu)))
   '(just-one-space)
   (p "<delay>: " tdel 'noinsert)
   (if (string-equal (tempo-lookup-named 'tdel) "")
       (list 'l "tdel=0.0") ;; default value
     (list 'l "tdel=" '(s tdel)))
   'n)
 "trigen"
 "template for inserting an ELDO triangular waveform generator"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-amm"
 '("Y"
   (p "[instance name]: ") " amm "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " "
   (p "[positive output]: ") " "
   (p "[negative output]: ") " param: "
   (p "<level (1/2)>: " level 'noinsert)
   (if (string-equal (tempo-lookup-named 'level) "")
       (list 'l "level=1")  ;; default value
     (list 'l "level=" '(s level)))
   '(just-one-space)
   (p "<slewrate (v/µs)>: " slr 'noinsert)
   (if (string-equal (tempo-lookup-named 'slr) "")
       (list 'l "slr=10")   ;; default value
     (list 'l "slr=" '(s slr)))
   '(just-one-space)
   (p "<offset voltage>: " voff 'noinsert)
   (if (string-equal (tempo-lookup-named 'voff) "")
       (list 'l "voff=0.0") ;; default value
     (list 'l "voff=" '(s voff)))
   '(just-one-space)
   (p "<carrier frequency>: " fc 'noinsert)
   (if (string-equal (tempo-lookup-named 'fc) "")
       (list 'l "fc=1e6")   ;; default value
     (list 'l "fc=" '(s fc)))
   '(just-one-space)
   (p "<minimal number of sampling points per period>: " nsam 'noinsert)
   (if (string-equal (tempo-lookup-named 'nsam) "")
       (list 'l "nsam=10")  ;; default value
     (list 'l "nsam=" '(s nsam)))
   'n)
 "amm"
 "template for inserting an ELDO amplitude modulator"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-pam"
 '("Y"
   (p "[instance name]: ") " pam "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " "
   (p "[positive output]: ") " "
   (p "[negative output]: ") " param: "
   (p "<level (1/2)>: " level 'noinsert)
   (if (string-equal (tempo-lookup-named 'level) "")
       (list 'l "level=1")  ;; default value
     (list 'l "level=" '(s level)))
   '(just-one-space)
   (p "<slewrate (v/µs)>: " slr 'noinsert)
   (if (string-equal (tempo-lookup-named 'slr) "")
       (list 'l "slr=10")   ;; default value
     (list 'l "slr=" '(s slr)))
   '(just-one-space)
   (p "<offset voltage>: " voff 'noinsert)
   (if (string-equal (tempo-lookup-named 'voff) "")
       (list 'l "voff=0.0") ;; default value
     (list 'l "voff=" '(s voff)))
   '(just-one-space)
   (p "<carrier frequency>: " fc 'noinsert)
   (if (string-equal (tempo-lookup-named 'fc) "")
       (list 'l "fc=1e6")   ;; default value
     (list 'l "fc=" '(s fc)))
   '(just-one-space)
   (p "<minimal number of sampling points per period>: " nsam 'noinsert)
   (if (string-equal (tempo-lookup-named 'nsam) "")
       (list 'l "nsam=10")  ;; default value
     (list 'l "nsam=" '(s nsam)))
   'n)
 "pam"
 "template for inserting an ELDO pulse amplitude modulator"
 'eldo-tempo-tags)


(tempo-define-template
 "eldo-saho"
 '("Y"
   (p "[instance name]: ") " sa_ho "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " "
   (p "[positive output]: ") " "
   (p "[negative output]: ") " param: "
   (p "<sampling frequency>: " fs 'noinsert)
   (if (string-equal (tempo-lookup-named 'fs) "")
       (list 'l "fs=1e6")   ;; default value
     (list 'l "fs=" '(s fs)))
   '(just-one-space)
   (p "<acquisition time>: " tacq 'noinsert)
   (if (string-equal (tempo-lookup-named 'tacq) "")
       (list 'l "tacq=1e-9")    ;; default value
     (list 'l "tacq=" '(s tacq)))
   '(just-one-space)
   (p "<droop voltage>: " dv 'noinsert)
   (if (string-equal (tempo-lookup-named 'dv) "")
       (list 'l "dv=20mv")  ;; default value
     (list 'l "dv=" '(s dv)))
   '(just-one-space)
   'n)
 "saho"
 "template for inserting an ELDO sample&hold"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-trho"
 '("Y"
   (p "[instance name]: ") " tr_ho "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " "
   (p "[positive output]: ") " "
   (p "[negative output]: ") " "
   (p "[controlling node]: ") " param: "
   (p "<threshold voltage for crt>: " vth 'noinsert)
   (if (string-equal (tempo-lookup-named 'vth) "")
       (list 'l "vth=0.5")  ;; default value
     (list 'l "vth=" '(s vth)))
   '(just-one-space)
   (p "<acquisition time>: " tacq 'noinsert)
   (if (string-equal (tempo-lookup-named 'tacq) "")
       (list 'l "tacq=1e-9")    ;; default value
     (list 'l "tacq=" '(s tacq)))
   'n)
 "trho"
 "template for inserting an ELDO track&hold"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-peakd"
 '("Y"
   (p "[instance name]: ") " peak_d "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " "
   (p "[positive output]: ") " "
   (p "[negative output]: ") " "
   (p "[controlling node]: ") " param: "
   (p "<level (1/2)>: " level 'noinsert)
   (if (string-equal (tempo-lookup-named 'level) "")
       (list 'l "level=1")  ;; default value
     (list 'l "level=2" ))
   '(just-one-space)
   (p "<threshold voltage for crt>: " vth 'noinsert)
   (if (string-equal (tempo-lookup-named 'vth) "")
       (list 'l "vth=0.5")  ;; default value
     (list 'l "vth=" '(s vth)))
   '(just-one-space)
   (p "<threshold voltage for reset on output>: " res 'noinsert)
   (if (string-equal (tempo-lookup-named 'res) "")
       (list 'l "res=0.5")  ;; default value
     (list 'l "res=" '(s res)))
   '(just-one-space)
   (p "<output slewrate (v/µs)>: " slr 'noinsert)
   (if (string-equal (tempo-lookup-named 'slr) "")
       (list 'l "slr=1.0")  ;; default value
     (list 'l "slr=" '(s slr)))
   '(just-one-space)
   (p "<output slewrate on reset>: " rslr 'noinsert)
   (if (string-equal (tempo-lookup-named 'rslr) "")
       (list 'l "rslr=1.0") ;; default value
     (list 'l "rslr=" '(s rslr)))
   'n)
 "peakd"
 "template for inserting an ELDO peak detector"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-levdso"
 '("Y"
   (p "[instance name]: ") " lev_d "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " "
   (p "[positive output]: ") " "
   (p "[negative output]: ") " param: "
   (p "<rise time (µs)>: " tr 'noinsert)
   (if (string-equal (tempo-lookup-named 'tr) "")
       (list 'l "tr=1.0")   ;; default value
     (list 'l "tr=" '(s tr)))
   '(just-one-space)
   (p "<fall time (µs)>: " tf 'noinsert)
   (if (string-equal (tempo-lookup-named 'tf) "")
       (list 'l "tf=1.0")   ;; default value
     (list 'l "tf=" '(s tf)))
   '(just-one-space)
   (p "<transit time (s)>: " tpd 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpd) "")
       (list 'l "tpd=0.0")  ;; default value
     (list 'l "tpd=" '(s tpd)))
   '(just-one-space)
   (p "<lower voltage level>: " v0 'noinsert)
   (if (string-equal (tempo-lookup-named 'v0) "")
       (list 'l "v0=0.0")   ;; default value
     (list 'l "v0=" '(s v0)))
   '(just-one-space)
   (p "<higher voltage level>: " v1 'noinsert)
   (if (string-equal (tempo-lookup-named 'v1) "")
       (list 'l "v1=1.0")   ;; default value
     (list 'l "v1=" '(s v1)))
   '(just-one-space)
   (p "<input offset voltage>: " voff 'noinsert)
   (if (string-equal (tempo-lookup-named 'voff) "")
       (list 'l "voff=0.0") ;; default value
     (list 'l "voff=" '(s voff)))
   '(just-one-space)
   (p "<lower reference voltage>: " vrl 'noinsert)
   (if (string-equal (tempo-lookup-named 'vrl) "")
       (list 'l "vrl=-0.1") ;; default value
     (list 'l "vrl=" '(s vrl)))
   '(just-one-space)
   (p "<higher reference voltage>: " vru 'noinsert)
   (if (string-equal (tempo-lookup-named 'vru) "")
       (list 'l "vru=0.1")  ;; default value
     (list 'l "vru=" '(s vru)))
   'n)
 "levdso"
 "template for inserting an ELDO single-output level detector"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-levddo"
 '("Y"
   (p "[instance name]: ") " lev_d "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " "
   (p "[positive output]: ") " "
   (p "[negative output]: ") " "
   (p "[reference node]: ") " param: "
   (p "<rise time (µs)>: " tr 'noinsert)
   (if (string-equal (tempo-lookup-named 'tr) "")
       (list 'l "tr=1.0")   ;; default value
     (list 'l "tr=" '(s tr)))
   '(just-one-space)
   (p "<fall time (µs)>: " tf 'noinsert)
   (if (string-equal (tempo-lookup-named 'tf) "")
       (list 'l "tf=1.0")   ;; default value
     (list 'l "tf=" '(s tf)))
   '(just-one-space)
   (p "<transit time (s)>: " tpd 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpd) "")
       (list 'l "tpd=0.0")  ;; default value
     (list 'l "tpd=" '(s tpd)))
   '(just-one-space)
   (p "<lower voltage level>: " v0 'noinsert)
   (if (string-equal (tempo-lookup-named 'v0) "")
       (list 'l "v0=0.0")   ;; default value
     (list 'l "v0=" '(s v0)))
   '(just-one-space)
   (p "<higher voltage level>: " v1 'noinsert)
   (if (string-equal (tempo-lookup-named 'v1) "")
       (list 'l "v1=1.0")   ;; default value
     (list 'l "v1=" '(s v1)))
   '(just-one-space)
   (p "<input offset voltage>: " voff 'noinsert)
   (if (string-equal (tempo-lookup-named 'voff) "")
       (list 'l "voff=0.0") ;; default value
     (list 'l "voff=" '(s voff)))
   '(just-one-space)
   (p "<lower reference voltage>: " vrl 'noinsert)
   (if (string-equal (tempo-lookup-named 'vrl) "")
       (list 'l "vrl=-0.1") ;; default value
     (list 'l "vrl=" '(s vrl)))
   '(just-one-space)
   (p "<higher reference voltage>: " vru 'noinsert)
   (if (string-equal (tempo-lookup-named 'vru) "")
       (list 'l "vru=0.1")  ;; default value
     (list 'l "vru=" '(s vru)))
   'n)
 "levddo"
 "template for inserting an ELDO differential-output level detector"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-logamp"
 '("Y"
   (p "[instance name]: ") " logamp "
   (p "[input]: ") " "
   (p "[output]: ") " param: "
   (p "<gain>: " gain 'noinsert)
   (if (string-equal (tempo-lookup-named 'gain) "")
       (list 'l "k=1.0")    ;; default value
     (list 'l "k=" '(s gain)))
   '(just-one-space)
   (p "<log function argument>: " e 'noinsert)
   (if (string-equal (tempo-lookup-named 'e) "")
       (list 'l "e=1")  ;; default value
     (list 'l "e=" '(s vmin)))
   '(just-one-space)
   (p "<vmax>: " vmax 'noinsert)
   (if (string-equal (tempo-lookup-named 'vmax) "")
       (list 'l "vmax=5.0") ;; default value
     (list 'l "vmax=" '(s vmax)))
   '(just-one-space)
   (p "<vmin>: " vmin 'noinsert)
   (if (string-equal (tempo-lookup-named 'vmin) "")
       (list 'l "vmin=-5.0")    ;; default value
     (list 'l "vmin=" '(s vmin)))
   '(just-one-space)
   (p "<positive saturation voltage>: " vsatp 'noinsert)
   (if (string-equal (tempo-lookup-named 'vsatp) "")
       (list 'l "vsatp=4.75")   ;; default value
     (list 'l "vsatp=" '(s vsatp)))
   '(just-one-space)
   (p "<negative saturation voltage>: " vsatn 'noinsert)
   (if (string-equal (tempo-lookup-named 'vsatn) "")
       (list 'l "vsatn=-4.75")  ;; default value
     (list 'l "vsatn=" '(s vsatn)))
   '(just-one-space)
   (p "<slope at vsatp>: " pslope 'noinsert)
   (if (string-equal (tempo-lookup-named 'pslope) "")
       (list 'l "pslope=0.25")  ;; default value
     (list 'l "pslope=" '(s pslope)))
   '(just-one-space)
   (p "<slope at vsatn>: " nslope 'noinsert)
   (if (string-equal (tempo-lookup-named 'nslope) "")
       (list 'l "nslope=0.25")  ;; default value
     (list 'l "nslope=" '(s nslope)))
   'n)
 "logamp"
 "template for inserting an ELDO logarithmic amplifier"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-antilog"
 '("Y"
   (p "[instance name]: ") " expamp "
   (p "[input]: ") " "
   (p "[output]: ") " param: "
   (p "<gain>: " k 'noinsert)
   (if (string-equal (tempo-lookup-named 'k) "")
       (list 'l "k=1.0")    ;; default value
     (list 'l "k=" '(s k)))
   '(just-one-space)
   (p "<exp function argument>: " e 'noinsert)
   (if (string-equal (tempo-lookup-named 'e) "")
       (list 'l "e=1")      ;; default value
     (list 'l "e=" '(s vmin)))
   '(just-one-space)
   (p "<base of power function>: " base 'noinsert)
   (if (string-equal (tempo-lookup-named 'base) "")
       (list 'l "base={exp(1)}")    ;; default value
     (list 'l "base=" '(s base)))
   '(just-one-space)
   (p "<vmax>: " vmax 'noinsert)
   (if (string-equal (tempo-lookup-named 'vmax) "")
       (list 'l "vmax=5.0") ;; default value
     (list 'l "vmax=" '(s vmax)))
   '(just-one-space)
   (p "<vmin>: " vmin 'noinsert)
   (if (string-equal (tempo-lookup-named 'vmin) "")
       (list 'l "vmin=-5.0")    ;; default value
     (list 'l "vmin=" '(s vmin)))
   '(just-one-space)
   (p "<positive saturation voltage>: " vsatp 'noinsert)
   (if (string-equal (tempo-lookup-named 'vsatp) "")
       (list 'l "vsatp=4.75")   ;; default value
     (list 'l "vsatp=" '(s vsatp)))
   '(just-one-space)
   (p "<negative saturation voltage>: " vsatn 'noinsert)
   (if (string-equal (tempo-lookup-named 'vsatn) "")
       (list 'l "vsatn=-4.75")  ;; default value
     (list 'l "vsatn=" '(s vsatn)))
   '(just-one-space)
   (p "<slope at vsatp>: " pslope 'noinsert)
   (if (string-equal (tempo-lookup-named 'pslope) "")
       (list 'l "pslope=0.25")  ;; default value
     (list 'l "pslope=" '(s pslope)))
   '(just-one-space)
   (p "<slope at vsatn>: " nslope 'noinsert)
   (if (string-equal (tempo-lookup-named 'nslope) "")
       (list 'l "nslope=0.25")  ;; default value
     (list 'l "nslope=" '(s nslope)))
   'n)
 "expamp"
 "template for inserting an ELDO anti-logarithmic amplifier"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-diff"
 '("Y"
   (p "[instance name]: ") " diff "
   (p "[input]: ") " "
   (p "[output]: ") " param: "
   (p "<time constant>: " k 'noinsert)
   (if (string-equal (tempo-lookup-named 'k) "")
       (list 'l "k=1")      ;; default value
     (list 'l "k=" '(s k)))
   '(just-one-space)
   (p "<dc value>: " c0 'noinsert)
   (if (string-equal (tempo-lookup-named 'c0) "")
       (list 'l "c0=1")     ;; default value
     (list 'l "c0=" '(s c0)))
   '(just-one-space)
   (p "<slewrate (v/s)>: " slr 'noinsert)
   (if (string-equal (tempo-lookup-named 'slr) "")
       (list 'l "slr=1e9")  ;; default value
     (list 'l "slr=" '(s slr)))
   'n)
 "diff"
 "template for inserting an ELDO differentiator"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-integ"
 '("Y"
   (p "[instance name]: ") " integ "
   (p "[input]: ") " "
   (p "[output]: ") " param: "
   (p "<time constant>: " k 'noinsert)
   (if (string-equal (tempo-lookup-named 'k) "")
       (list 'l "k=1")      ;; default value
     (list 'l "k=" '(s k)))
   '(just-one-space)
   (p "<dc value>: " c0 'noinsert)
   (if (string-equal (tempo-lookup-named 'c0) "")
       (list 'l "c0=1")     ;; default value
     (list 'l "c0=" '(s c0)))
   'n)
 "integ"
 "template for inserting an ELDO integrator"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-adder"
 '("y"
   (p "[instance name]: ")  " "
   (p "<add/sub/mult/div>: ") " "
   (p "[input 1]: ") " "
   (p "[input 2]: ") " "
   (p "[output]: ") " param: "
   (p "<vmax>: " vmax 'noinsert)
   (if (string-equal (tempo-lookup-named 'vmax) "")
       (list 'l "vmax=5.0") ;; default value
     (list 'l "vmax=" '(s vmax)))
   '(just-one-space)
   (p "<vmin>: " vmin 'noinsert)
   (if (string-equal (tempo-lookup-named 'vmin) "")
       (list 'l "vmin=-5.0")    ;; default value
     (list 'l "vmin=" '(s vmin)))
   '(just-one-space)
   (p "<positive saturation voltage>: " vsatp 'noinsert)
   (if (string-equal (tempo-lookup-named 'vsatp) "")
       (list 'l "vsatp=4.75")   ;; default value
     (list 'l "vsatp=" '(s vsatp)))
   '(just-one-space)
   (p "<negative saturation voltage>: " vsatn 'noinsert)
   (if (string-equal (tempo-lookup-named 'vsatn) "")
       (list 'l "vsatn=-4.75")  ;; default value
     (list 'l "vsatn=" '(s vsatn)))
   '(just-one-space)
   (p "<slope at vsatp>: " pslope 'noinsert)
   (if (string-equal (tempo-lookup-named 'pslope) "")
       (list 'l "pslope=1.0")  ;; default value
     (list 'l "pslope=" '(s pslope)))
   '(just-one-space)
   (p "<slope at vsatn>: " nslope 'noinsert)
   (if (string-equal (tempo-lookup-named 'nslope) "")
       (list 'l "nslope=1.0")  ;; default value
     (list 'l "nslope=" '(s nslope)))
    '(just-one-space)
   (p "<multiplier>: " M 'noinsert)
   (if (string-equal (tempo-lookup-named 'M) "")
       (list 'l "m=1.0")  ;; default value
     (list 'l "m=" '(s m)))
 'n)
 "add"
 "template for inserting an ELDO adder/subtrator/multiplier/divider"
 'eldo-tempo-tags)


;; -------------------
;; Digital Macromodels
;; -------------------

(tempo-define-template
 "eldo-inv"
 '("INV"
   (p "[instance name]: ") " "
   (p "[input]: ") " "
   (p "[output]: ") " "
   (p "[model name]: ") " "
   (p "<vhigh>: " vhi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vhi) "")
       (list 'l "vhi=5.0")  ;; default value
     (list 'l "vhi=" '(s vhi)))
   '(just-one-space)
   (p "<vlow>: " vlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vlo) "")
       (list 'l "vlo=0.0")  ;; default value
     (list 'l "vlo=" '(s vlo)))
   '(just-one-space)
   (p "<threshold input voltage>: " vth 'noinsert)
   (if (string-equal (tempo-lookup-named 'vth) "")
       (list 'l "vth=2.5")  ;; default value
     (list 'l "vth=" '(s vth)))
   '(just-one-space)
   (p "<threshold input voltage rising edge>: " vthi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vthi) "")
       (list 'l "") ;; default value
     (list 'l "vthi=" '(s vthi)))
   '(just-one-space)
   (p "<threshold input voltage falling edge>: " vtlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vtlo) "")
       (list 'l "") ;; default value
     (list 'l "vtlo=" '(s vtlo)))
   '(just-one-space)
   (p "<transit time>: " tpd 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpd) "")
       (list 'l "tpd=1.0ns")    ;; default value
     (list 'l "tpd=" '(s tpd)))
   '(just-one-space)
   (p "<transit time for output to reach vthi>: " tpdup 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpdup) "")
       (list 'l "") ;; default value
     (list 'l "tpdup=" '(s tpdup)))
   '(just-one-space)
   (p "<transit time for output to reach vtlo>: " tpdown 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpdown) "")
       (list 'l "") ;; default value
     (list 'l "tpdown=" '(s tpdown)))
   '(just-one-space)
   (p "<input capacitance>: " cin 'noinsert)
   (if (string-equal (tempo-lookup-named 'cin) "")
       (list 'l "cin=0.0")  ;; default value
     (list 'l "cin=" '(s cin)))
   '(just-one-space)
   'n)
 "inv"
 "template for inserting an ELDO INVERTER gate macromodel"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-xor"
 '("XOR"
   (p "[instance name]: ") " "
   (p "[first input]: ") " "
   (p "[second input]: ") " "
   (p "[output]: ") " "
   (p "[model name]: ") " "
   (p "<vhigh>: " vhi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vhi) "")
       (list 'l "vhi=5.0")  ;; default value
     (list 'l "vhi=" '(s vhi)))
   '(just-one-space)
   (p "<vlow>: " vlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vlo) "")
       (list 'l "vlo=0.0")  ;; default value
     (list 'l "vlo=" '(s vlo)))
   '(just-one-space)
   (p "<threshold input voltage>: " vth 'noinsert)
   (if (string-equal (tempo-lookup-named 'vth) "")
       (list 'l "vth=2.5")  ;; default value
     (list 'l "vth=" '(s vth)))
   '(just-one-space)
   (p "<threshold input voltage rising edge>: " vthi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vthi) "")
       (list 'l "") ;; default value
     (list 'l "vthi=" '(s vthi)))
   '(just-one-space)
   (p "<threshold input voltage falling edge>: " vtlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vtlo) "")
       (list 'l "") ;; default value
     (list 'l "vtlo=" '(s vtlo)))
   '(just-one-space)
   (p "<transit time>: " tpd 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpd) "")
       (list 'l "tpd=1.0ns")    ;; default value
     (list 'l "tpd=" '(s tpd)))
   '(just-one-space)
   (p "<transit time for output to reach vthi>: " tpdup 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpdup) "")
       (list 'l "") ;; default value
     (list 'l "tpdup=" '(s tpdup)))
   '(just-one-space)
   (p "<transit time for output to reach vtlo>: " tpdown 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpdown) "")
       (list 'l "") ;; default value
     (list 'l "tpdown=" '(s tpdown)))
   '(just-one-space)
   (p "<input capacitance>: " cin 'noinsert)
   (if (string-equal (tempo-lookup-named 'cin) "")
       (list 'l "cin=0.0")  ;; default value
     (list 'l "cin=" '(s cin)))
   '(just-one-space)
   'n)
 "xor"
 "template for inserting an ELDO Exclusive-OR gate macromodel"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-and2"
 '("AND"
   (p "[instance name]: ") " "
   (p "[first input]: ") " "
   (p "[second input]: ") " "
   (p "[output]: ") " "
   (p "[model name]: ") " "
   (p "<vhigh>: " vhi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vhi) "")
       (list 'l "vhi=5.0")  ;; default value
     (list 'l "vhi=" '(s vhi)))
   '(just-one-space)
   (p "<vlow>: " vlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vlo) "")
       (list 'l "vlo=0.0")  ;; default value
     (list 'l "vlo=" '(s vlo)))
   '(just-one-space)
   (p "<threshold input voltage>: " vth 'noinsert)
   (if (string-equal (tempo-lookup-named 'vth) "")
       (list 'l "vth=2.5")  ;; default value
     (list 'l "vth=" '(s vth)))
   '(just-one-space)
   (p "<threshold input voltage rising edge>: " vthi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vthi) "")
       (list 'l "") ;; default value
     (list 'l "vthi=" '(s vthi)))
   '(just-one-space)
   (p "<threshold input voltage falling edge>: " vtlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vtlo) "")
       (list 'l "") ;; default value
     (list 'l "vtlo=" '(s vtlo)))
   '(just-one-space)
   (p "<transit time>: " tpd 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpd) "")
       (list 'l "tpd=1.0ns")    ;; default value
     (list 'l "tpd=" '(s tpd)))
   '(just-one-space)
   (p "<transit time for output to reach vthi>: " tpdup 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpdup) "")
       (list 'l "") ;; default value
     (list 'l "tpdup=" '(s tpdup)))
   '(just-one-space)
   (p "<transit time for output to reach vtlo>: " tpdown 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpdown) "")
       (list 'l "") ;; default value
     (list 'l "tpdown=" '(s tpdown)))
   '(just-one-space)
   (p "<input capacitance>: " cin 'noinsert)
   (if (string-equal (tempo-lookup-named 'cin) "")
       (list 'l "cin=0.0")  ;; default value
     (list 'l "cin=" '(s cin)))
   '(just-one-space)
   'n)
 "and"
 "template for inserting an ELDO 2 input AND gate macromodel"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-nand2"
 '("NAND"
   (p "[instance name]: ") " "
   (p "[first input]: ") " "
   (p "[second input]: ") " "
   (p "[output]: ") " "
   (p "[model name]: ") " "
   (p "<vhigh>: " vhi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vhi) "")
       (list 'l "vhi=5.0")  ;; default value
     (list 'l "vhi=" '(s vhi)))
   '(just-one-space)
   (p "<vlow>: " vlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vlo) "")
       (list 'l "vlo=0.0")  ;; default value
     (list 'l "vlo=" '(s vlo)))
   '(just-one-space)
   (p "<threshold input voltage>: " vth 'noinsert)
   (if (string-equal (tempo-lookup-named 'vth) "")
       (list 'l "vth=2.5")  ;; default value
     (list 'l "vth=" '(s vth)))
   '(just-one-space)
   (p "<threshold input voltage rising edge>: " vthi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vthi) "")
       (list 'l "") ;; default value
     (list 'l "vthi=" '(s vthi)))
   '(just-one-space)
   (p "<threshold input voltage falling edge>: " vtlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vtlo) "")
       (list 'l "") ;; default value
     (list 'l "vtlo=" '(s vtlo)))
   '(just-one-space)
   (p "<transit time>: " tpd 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpd) "")
       (list 'l "tpd=1.0ns")    ;; default value
     (list 'l "tpd=" '(s tpd)))
   '(just-one-space)
   (p "<transit time for output to reach vthi>: " tpdup 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpdup) "")
       (list 'l "") ;; default value
     (list 'l "tpdup=" '(s tpdup)))
   '(just-one-space)
   (p "<transit time for output to reach vtlo>: " tpdown 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpdown) "")
       (list 'l "") ;; default value
     (list 'l "tpdown=" '(s tpdown)))
   '(just-one-space)
   (p "<input capacitance>: " cin 'noinsert)
   (if (string-equal (tempo-lookup-named 'cin) "")
       (list 'l "cin=0.0")  ;; default value
     (list 'l "cin=" '(s cin)))
   '(just-one-space)
   'n)
 "nand"
 "template for inserting an ELDO 2 input NAND gate macromodel"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-or2"
 '("OR"
   (p "[instance name]: ") " "
   (p "[first input]: ") " "
   (p "[second input]: ") " "
   (p "[output]: ") " "
   (p "[model name]: ") " "
   (p "<vhigh>: " vhi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vhi) "")
       (list 'l "vhi=5.0")  ;; default value
     (list 'l "vhi=" '(s vhi)))
   '(just-one-space)
   (p "<vlow>: " vlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vlo) "")
       (list 'l "vlo=0.0")  ;; default value
     (list 'l "vlo=" '(s vlo)))
   '(just-one-space)
   (p "<threshold input voltage>: " vth 'noinsert)
   (if (string-equal (tempo-lookup-named 'vth) "")
       (list 'l "vth=2.5")  ;; default value
     (list 'l "vth=" '(s vth)))
   '(just-one-space)
   (p "<threshold input voltage rising edge>: " vthi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vthi) "")
       (list 'l "") ;; default value
     (list 'l "vthi=" '(s vthi)))
   '(just-one-space)
   (p "<threshold input voltage falling edge>: " vtlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vtlo) "")
       (list 'l "") ;; default value
     (list 'l "vtlo=" '(s vtlo)))
   '(just-one-space)
   (p "<transit time>: " tpd 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpd) "")
       (list 'l "tpd=1.0ns")    ;; default value
     (list 'l "tpd=" '(s tpd)))
   '(just-one-space)
   (p "<transit time for output to reach vthi>: " tpdup 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpdup) "")
       (list 'l "") ;; default value
     (list 'l "tpdup=" '(s tpdup)))
   '(just-one-space)
   (p "<transit time for output to reach vtlo>: " tpdown 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpdown) "")
       (list 'l "") ;; default value
     (list 'l "tpdown=" '(s tpdown)))
   '(just-one-space)
   (p "<input capacitance>: " cin 'noinsert)
   (if (string-equal (tempo-lookup-named 'cin) "")
       (list 'l "cin=0.0")  ;; default value
     (list 'l "cin=" '(s cin)))
   '(just-one-space)
   'n)
 "or"
 "template for inserting an ELDO 2 input OR gate macromodel"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-nor2"
 '("NOR"
   (p "[instance name]: ") " "
   (p "[first input]: ") " "
   (p "[second input]: ") " "
   (p "[output]: ") " "
   (p "[model name]: ") " "
   (p "<vhigh>: " vhi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vhi) "")
       (list 'l "vhi=5.0")  ;; default value
     (list 'l "vhi=" '(s vhi)))
   '(just-one-space)
   (p "<vlow>: " vlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vlo) "")
       (list 'l "vlo=0.0")  ;; default value
     (list 'l "vlo=" '(s vlo)))
   '(just-one-space)
   (p "<threshold input voltage>: " vth 'noinsert)
   (if (string-equal (tempo-lookup-named 'vth) "")
       (list 'l "vth=2.5")  ;; default value
     (list 'l "vth=" '(s vth)))
   '(just-one-space)
   (p "<threshold input voltage rising edge>: " vthi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vthi) "")
       (list 'l "") ;; default value
     (list 'l "vthi=" '(s vthi)))
   '(just-one-space)
   (p "<threshold input voltage falling edge>: " vtlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vtlo) "")
       (list 'l "") ;; default value
     (list 'l "vtlo=" '(s vtlo)))
   '(just-one-space)
   (p "<transit time>: " tpd 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpd) "")
       (list 'l "tpd=1.0ns")    ;; default value
     (list 'l "tpd=" '(s tpd)))
   '(just-one-space)
   (p "<transit time for output to reach vthi>: " tpdup 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpdup) "")
       (list 'l "") ;; default value
     (list 'l "tpdup=" '(s tpdup)))
   '(just-one-space)
   (p "<transit time for output to reach vtlo>: " tpdown 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpdown) "")
       (list 'l "") ;; default value
     (list 'l "tpdown=" '(s tpdown)))
   '(just-one-space)
   (p "<input capacitance>: " cin 'noinsert)
   (if (string-equal (tempo-lookup-named 'cin) "")
       (list 'l "cin=0.0")  ;; default value
     (list 'l "cin=" '(s cin)))
   '(just-one-space)
   'n)
 "nor"
 "template for inserting an ELDO 2 input NOR gate macromodel"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-and3"
 '("AND3"
   (p "[instance name]: ") " "
   (p "[first input]: ") " "
   (p "[second input]: ") " "
   (p "[output]: ") " "
   (p "[model name]: ") " "
   (p "<vhigh>: " vhi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vhi) "")
       (list 'l "vhi=5.0")  ;; default value
     (list 'l "vhi=" '(s vhi)))
   '(just-one-space)
   (p "<vlow>: " vlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vlo) "")
       (list 'l "vlo=0.0")  ;; default value
     (list 'l "vlo=" '(s vlo)))
   '(just-one-space)
   (p "<threshold input voltage>: " vth 'noinsert)
   (if (string-equal (tempo-lookup-named 'vth) "")
       (list 'l "vth=2.5")  ;; default value
     (list 'l "vth=" '(s vth)))
   '(just-one-space)
   (p "<threshold input voltage rising edge>: " vthi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vthi) "")
       (list 'l "") ;; default value
     (list 'l "vthi=" '(s vthi)))
   '(just-one-space)
   (p "<threshold input voltage falling edge>: " vtlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vtlo) "")
       (list 'l "") ;; default value
     (list 'l "vtlo=" '(s vtlo)))
   '(just-one-space)
   (p "<transit time>: " tpd 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpd) "")
       (list 'l "tpd=1.0ns")    ;; default value
     (list 'l "tpd=" '(s tpd)))
   '(just-one-space)
   (p "<transit time for output to reach vthi>: " tpdup 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpdup) "")
       (list 'l "") ;; default value
     (list 'l "tpdup=" '(s tpdup)))
   '(just-one-space)
   (p "<transit time for output to reach vtlo>: " tpdown 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpdown) "")
       (list 'l "") ;; default value
     (list 'l "tpdown=" '(s tpdown)))
   '(just-one-space)
   (p "<input capacitance>: " cin 'noinsert)
   (if (string-equal (tempo-lookup-named 'cin) "")
       (list 'l "cin=0.0")  ;; default value
     (list 'l "cin=" '(s cin)))
   '(just-one-space)
   'n)
 "and3"
 "template for inserting an ELDO 3 input AND gate macromodel"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-nand3"
 '("NAND3"
   (p "[instance name]: ") " "
   (p "[first input]: ") " "
   (p "[second input]: ") " "
   (p "[output]: ") " "
   (p "[model name]: ") " "
   (p "<vhigh>: " vhi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vhi) "")
       (list 'l "vhi=5.0")  ;; default value
     (list 'l "vhi=" '(s vhi)))
   '(just-one-space)
   (p "<vlow>: " vlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vlo) "")
       (list 'l "vlo=0.0")  ;; default value
     (list 'l "vlo=" '(s vlo)))
   '(just-one-space)
   (p "<threshold input voltage>: " vth 'noinsert)
   (if (string-equal (tempo-lookup-named 'vth) "")
       (list 'l "vth=2.5")  ;; default value
     (list 'l "vth=" '(s vth)))
   '(just-one-space)
   (p "<threshold input voltage rising edge>: " vthi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vthi) "")
       (list 'l "") ;; default value
     (list 'l "vthi=" '(s vthi)))
   '(just-one-space)
   (p "<threshold input voltage falling edge>: " vtlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vtlo) "")
       (list 'l "") ;; default value
     (list 'l "vtlo=" '(s vtlo)))
   '(just-one-space)
   (p "<transit time>: " tpd 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpd) "")
       (list 'l "tpd=1.0ns")    ;; default value
     (list 'l "tpd=" '(s tpd)))
   '(just-one-space)
   (p "<transit time for output to reach vthi>: " tpdup 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpdup) "")
       (list 'l "") ;; default value
     (list 'l "tpdup=" '(s tpdup)))
   '(just-one-space)
   (p "<transit time for output to reach vtlo>: " tpdown 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpdown) "")
       (list 'l "") ;; default value
     (list 'l "tpdown=" '(s tpdown)))
   '(just-one-space)
   (p "<input capacitance>: " cin 'noinsert)
   (if (string-equal (tempo-lookup-named 'cin) "")
       (list 'l "cin=0.0")  ;; default value
     (list 'l "cin=" '(s cin)))
   '(just-one-space)
   'n)
 "nand3"
 "template for inserting an ELDO 3 input NAND gate macromodel"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-or3"
 '("OR3"
   (p "[instance name]: ") " "
   (p "[first input]: ") " "
   (p "[second input]: ") " "
   (p "[output]: ") " "
   (p "[model name]: ") " "
   (p "<vhigh>: " vhi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vhi) "")
       (list 'l "vhi=5.0")  ;; default value
     (list 'l "vhi=" '(s vhi)))
   '(just-one-space)
   (p "<vlow>: " vlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vlo) "")
       (list 'l "vlo=0.0")  ;; default value
     (list 'l "vlo=" '(s vlo)))
   '(just-one-space)
   (p "<threshold input voltage>: " vth 'noinsert)
   (if (string-equal (tempo-lookup-named 'vth) "")
       (list 'l "vth=2.5")  ;; default value
     (list 'l "vth=" '(s vth)))
   '(just-one-space)
   (p "<threshold input voltage rising edge>: " vthi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vthi) "")
       (list 'l "") ;; default value
     (list 'l "vthi=" '(s vthi)))
   '(just-one-space)
   (p "<threshold input voltage falling edge>: " vtlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vtlo) "")
       (list 'l "") ;; default value
     (list 'l "vtlo=" '(s vtlo)))
   '(just-one-space)
   (p "<transit time>: " tpd 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpd) "")
       (list 'l "tpd=1.0ns")    ;; default value
     (list 'l "tpd=" '(s tpd)))
   '(just-one-space)
   (p "<transit time for output to reach vthi>: " tpdup 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpdup) "")
       (list 'l "") ;; default value
     (list 'l "tpdup=" '(s tpdup)))
   '(just-one-space)
   (p "<transit time for output to reach vtlo>: " tpdown 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpdown) "")
       (list 'l "") ;; default value
     (list 'l "tpdown=" '(s tpdown)))
   '(just-one-space)
   (p "<input capacitance>: " cin 'noinsert)
   (if (string-equal (tempo-lookup-named 'cin) "")
       (list 'l "cin=0.0")  ;; default value
     (list 'l "cin=" '(s cin)))
   '(just-one-space)
   'n)
 "or3"
 "template for inserting an ELDO 3 input OR gate macromodel"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-nor3"
 '("NOR3"
   (p "[instance name]: ") " "
   (p "[first input]: ") " "
   (p "[second input]: ") " "
   (p "[output]: ") " "
   (p "[model name]: ") " "
   (p "<vhigh>: " vhi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vhi) "")
       (list 'l "vhi=5.0")  ;; default value
     (list 'l "vhi=" '(s vhi)))
   '(just-one-space)
   (p "<vlow>: " vlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vlo) "")
       (list 'l "vlo=0.0")  ;; default value
     (list 'l "vlo=" '(s vlo)))
   '(just-one-space)
   (p "<threshold input voltage>: " vth 'noinsert)
   (if (string-equal (tempo-lookup-named 'vth) "")
       (list 'l "vth=2.5")  ;; default value
     (list 'l "vth=" '(s vth)))
   '(just-one-space)
   (p "<threshold input voltage rising edge>: " vthi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vthi) "")
       (list 'l "") ;; default value
     (list 'l "vthi=" '(s vthi)))
   '(just-one-space)
   (p "<threshold input voltage falling edge>: " vtlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vtlo) "")
       (list 'l "") ;; default value
     (list 'l "vtlo=" '(s vtlo)))
   '(just-one-space)
   (p "<transit time>: " tpd 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpd) "")
       (list 'l "tpd=1.0ns")    ;; default value
     (list 'l "tpd=" '(s tpd)))
   '(just-one-space)
   (p "<transit time for output to reach vthi>: " tpdup 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpdup) "")
       (list 'l "") ;; default value
     (list 'l "tpdup=" '(s tpdup)))
   '(just-one-space)
   (p "<transit time for output to reach vtlo>: " tpdown 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpdown) "")
       (list 'l "") ;; default value
     (list 'l "tpdown=" '(s tpdown)))
   '(just-one-space)
   (p "<input capacitance>: " cin 'noinsert)
   (if (string-equal (tempo-lookup-named 'cin) "")
       (list 'l "cin=0.0")  ;; default value
     (list 'l "cin=" '(s cin)))
   '(just-one-space)
   'n)
 "nor3"
 "template for inserting an ELDO 3 input NOR gate macromodel"
 'eldo-tempo-tags)

;; -------------------------
;; Mixed signal Macromodels
;; -------------------------

(tempo-define-template
 "eldo-adc"
 '("ADC"
   (p "[instance name]: ") " "
   (p "[lock]: ") " "
   (p "[analog input]: ") " "
   (p "[digital outputs from MSB to LSB]: ") " "
   (p "<edge (1/-1)>: " edge 'noinsert)
   (if (string-equal (tempo-lookup-named 'edge) "")
       (list 'l "edge=1")   ;; default value
     (list 'l "edge=-1"))
   '(just-one-space)
   (p "<threshold clock voltage>: " vth 'noinsert)
   (if (string-equal (tempo-lookup-named 'vth) "")
       (list 'l "vth=2.5")  ;; default value
     (list 'l "vth=" '(s vth)))
   '(just-one-space)
   (p "<vhigh>: " vhi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vhi) "")
       (list 'l "vhi=5.0")  ;; default value
     (list 'l "vhi=" '(s vhi)))
   '(just-one-space)
   (p "<vlow>: " vlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vlo) "")
       (list 'l "vlo=0.0")  ;; default value
     (list 'l "vlo=" '(s vlo)))
   '(just-one-space)
   (p "<analog input lower voltage>: " vinf 'noinsert)
   (if (string-equal (tempo-lookup-named 'vinf) "")
       (list 'l "vinf=0.0") ;; default value
     (list 'l "vthi=" '(s vinf)))
   '(just-one-space)
   (p "<analog input higher voltage>: " vsup 'noinsert)
   (if (string-equal (tempo-lookup-named 'vsup) "")
       (list 'l "vsup=5.0") ;; default value
     (list 'l "vsup=" '(s vsup)))
   '(just-one-space)
   (p "<output bits commutation time>: " tcom 'noinsert)
   (if (string-equal (tempo-lookup-named 'tcom) "")
       (list 'l "tcom=1.0ns")   ;; default value
     (list 'l "tcom=" '(s tcom)))
   '(just-one-space)
   (p "<transit time>: " tpd 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpd) "")
       (list 'l "tpd=10ns") ;; default value
     (list 'l "tpd=" '(s tpd)))
   '(just-one-space)
   'n)
 "adc"
 "template for inserting an ELDO Analog to Digital Converter  macromodel"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-dac"
 '("DAC"
   (p "[instance name]: ") " "
   (p "[clock]: ") " "
   (p "[digital inputs from MSB to LSB]: ") " "
   (p "[analog output]: ") " "
   (p "<edge (1/-1)>: " edge 'noinsert)
   (if (string-equal (tempo-lookup-named 'edge) "")
       (list 'l "edge=1")   ;; default value
     (list 'l "edge=-1"))
   '(just-one-space)
   (p "<threshold clock voltage>: " vth 'noinsert)
   (if (string-equal (tempo-lookup-named 'vth) "")
       (list 'l "vth=2.5")  ;; default value
     (list 'l "vth=" '(s vth)))
   '(just-one-space)
   (p "<threshold input voltage>: " vtin 'noinsert)
   (if (string-equal (tempo-lookup-named 'vtin) "")
       (list 'l "vtin=2.5") ;; default value
     (list 'l "vtin=" '(s vtin)))
   '(just-one-space)
   (p "<analog output vhigh>: " vhi 'noinsert)
   (if (string-equal (tempo-lookup-named 'vhi) "")
       (list 'l "vhi=5.0")  ;; default value
     (list 'l "vhi=" '(s vhi)))
   '(just-one-space)
   (p "<analog output vlow>: " vlo 'noinsert)
   (if (string-equal (tempo-lookup-named 'vlo) "")
       (list 'l "vlo=0.0")  ;; default value
     (list 'l "vlo=" '(s vlo)))
   '(just-one-space)
   (p "<transit time>: " tpd 'noinsert)
   (if (string-equal (tempo-lookup-named 'tpd) "")
       (list 'l "tpd=10ns") ;; default value
     (list 'l "tpd=" '(s tpd)))
   '(just-one-space)
   (p "<output slope (v/s)>: " sl 'noinsert)
   (if (string-equal (tempo-lookup-named 'sl) "")
       (list 'l "sl=10e8")  ;; default value
     (list 'l "sl=" '(s tcom)))
   '(just-one-space)
   'n)
 "DAC"
 "template for inserting an ELDO Digital to Analog Converter macromodel"
 'eldo-tempo-tags)

;; -------------------------
;; Switched cap Macromodels
;; -------------------------

(tempo-define-template
 "eldo-switchcap-opa"
 '("OPA"
   (p "[instance name]: ") " "
   (p "[positive input]: ") " "
   (p "[negative input]: ") " "
   (p "[positive output]: ") " "
   (p "[negative output]: ") " "
   (p "[model name]: ") " "
   (p "<input offset>: " voff 'noinsert)
   (if (string-equal (tempo-lookup-named 'voff) "")
       (list 'l "voff=0.0") ;; default value
     (list 'l "voff=" '(s voff)))
   '(just-one-space)
   (p "<slew rate (v/s)>: " sl 'noinsert)
   (if (string-equal (tempo-lookup-named 'sl) "")
       (list 'l "sl=1e6")   ;; default value
     (list 'l "sl=" '(s voff)))
   '(just-one-space)
   (p "<gain>: " gain 'noinsert)
   (if (string-equal (tempo-lookup-named 'gain) "")
       (list 'l "gain=1e5") ;; default value
     (list 'l "gain=" '(s gain)))
   '(just-one-space)
   (p "<input capacitance>: " cin 'noinsert)
   (if (string-equal (tempo-lookup-named 'cin) "")
       (list 'l "cin=0")    ;; default value
     (list 'l "cin=" '(s cin)))
   '(just-one-space)
   (p "<output resistance>: " rs 'noinsert)
   (if (string-equal (tempo-lookup-named 'rs) "")
       (list 'l "rs=10e6")  ;; default value
     (list 'l "rs=" '(s rs)))
   '(just-one-space)
   (p "<symmetrical saturation voltage>: " vsat 'noinsert)
   (if (string-equal (tempo-lookup-named 'vsat) "")
       (list 'l "vsat=5.0") ;; default value
     (list 'l "vsat=" '(s vsat)))
   '(just-one-space)
   (p "<asymmetrical saturation voltage>: " vsatm 'noinsert)
   (if (string-equal (tempo-lookup-named 'vsatm) "")
       (list 'l "vsatm=5.0")    ;; default value
     (list 'l "vsatm=" '(s vsatm)))
   '(just-one-space)
   (p "<cutoff frequency (double stage only)>: " fc 'noinsert)
   (if (string-equal (tempo-lookup-named 'fc) "")
       (list 'l "fc=1k")    ;; default value
     (list 'l "fc=" '(s fc)))
   '(just-one-space)
   (p "<non-dominant pole (single stage only)>: " fndp 'noinsert)
   (if (string-equal (tempo-lookup-named 'fndp) "")
       (list 'l "fndp=1k")  ;; default value
     (list 'l "fndp=" '(s fndp)))
   '(just-one-space)
   (p "<max current>: " imax 'noinsert)
   (if (string-equal (tempo-lookup-named 'imax) "")
       (list 'l "imax=100ma")   ;; default value
     (list 'l "imax=" '(s imax)))
   '(just-one-space)
   (p "<common mode rejection ratio>: " cmrr 'noinsert)
   (if (string-equal (tempo-lookup-named 'cmrr) "")
       (list 'l "cmrr=0.0") ;; default value
     (list 'l "cmrr=" '(s cmrr)))
   'n)
 "opa"
 "template for inserting an ELDO differential single or double stage opamp"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-switch"
 '("S"
   (p "[instance name]: ") " "
   (p "[controlling node]: ") " "
   (p "[node 1]: ") " "
   (p "[node 2]: ") " "
   (p "[model name]: ") " "
   (p "<ron resistance>: " ron 'noinsert)
   (if (string-equal (tempo-lookup-named 'ron) "")
       (list 'l "ron=1k")   ;; default value
     (list 'l "ron=" '(s ron)))
   '(just-one-space)
   (p "<overlap capacitance>: " crec 'noinsert)
   (if (string-equal (tempo-lookup-named 'crec) "")
       (list 'l "crec=0")   ;; default value
     (list 'l "crec=" '(s crec)))
   'n)
 "switch"
 "template for inserting an ELDO switch macromodel"
 'eldo-tempo-tags)

;; -------------------
;; Extracts
;; -------------------

(tempo-define-template
 "eldo-phmag"
 '(".extract ac label=\"Phase margin\" xycond(vp("
   (p "[node]: " lname)
   "),vdb(" (s lname) ")<0.0)+180 "
   'n)
 "phmag"
 "template for extracting the phase margin"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-pmmin"
 '(".extract ac label=\"PM min\" min(vp("
   (p "[node]: " lname)
   "),0,xdown(vdb(" (s lname) "),0))+180 "
   'n)
 "pmmin"
 "template for extracting the minimal phase before unity gain frequency"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-gmag"
 '(".extract ac label=\"Gain margin\" -xycond(vdb("
   (p "[node]: " lname)
   "),vp(" (s lname) ")<-180) "
   'n)
 "gmag"
 "template for extracting the gain margin"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-fc"
 '(".extract ac label=\"Cut freq\" xdown(vdb("
   (p "[node]: " lname)
   "),yval(vdb(" (s lname) "),1)-3) "
   'n)
 "fc"
 "template for extracting the cut frequency"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-ugfc"
 '(".extract ac label=\"Unity gain freq\" xdown(vdb("
   (p "[node]: " lname)
   "),0) "
   'n)
 "ugfc"
 "template for extracting the unity gain frequency"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-bandgap-flatness"
 '(
   "! Temperature range:" n
   ".param Tmin=-40 Tamb=27 Tmax=140 Tmid={Tmax+Tmin}/2" n n
   ".dc temp {Tmin} {Tmax} 1" n n
   ".extract dcsweep label=\"VBG min value\" unit=Volt min(V(" (p "[node]: " lname) "))" n
   ".extract dcsweep label=\"VBG max value\" unit=Volt max(V(" (s lname) "))" n
   ".extract dcsweep label=\"VBG average value\" unit=Volt abs(average(V(" (s lname) ")))" n
   ".extract dcsweep label=\"VBG peak-to-peak value\"  unit=Volt (max(V(" (s lname) "))-min(V(" (s lname) ")))*sign(xmax(V(" (s lname) "))-xmin(V(" (s lname) ")))" n
   ".extract dcsweep label=\"VBG value@min temperature\" unit=Volt yval(V(" (s lname) "), {Tmin})" n
   ".extract dcsweep label=\"VBG value@mid temperature\" unit=Volt yval(V(" (s lname) "), {Tmid})" n
   ".extract dcsweep label=\"VBG value@max temperature\" unit=Volt yval(V(" (s lname) "), {Tmax})" n
   ".extract dcsweep label=\"VBG slope@min temperature (V/C)\" slope(V(" (s lname) "), yval(V(" (s lname) "), {Tmin}))" n
   ".extract dcsweep label=\"VBG slope@mid temperature (V/C)\" slope(V(" (s lname) "), yval(V(" (s lname) "), {Tmid}))" n
   ".extract dcsweep label=\"VBG slope@max temperature (V/C)\" slope(V(" (s lname) "), yval(V(" (s lname) "), {Tmax}))" n
   ".extract dcsweep label=\"VBG inflection temperature (C)\" unit=temp xmax(V(" (s lname) "))" n
   ".extract dcsweep label=\"VBG Temperature Coefficient (ppm/C)\" 1e6*(max(V(" (s lname) "))-min(V(" (s lname) ")))/abs(average(V(" (s lname) ")))/{Tmax-Tmin}" n
   'n)
 "bandgap-flatness"
 "template for extracting bandgap flatness caracteristics"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-period"
 '(".extract tran xdown(v("
   (p "[node]: " lname)
   "),"
   (p "[threshold]: " vth)
   ","
   (p "[estimation time]: " t)
   ",end)"
   "-xdown(v(" (s lname) "),"(s vth) ","(s t) ",start) !period"
   'n)
 "period"
 "template for extracting the period of a signal"
 'eldo-tempo-tags)


;; Some macros (defmac)

(tempo-define-template
 "eldo-period-macro"
 '(".defmac period(a,th,time)=xdown(a,th,time,end)"
   "-xdown(a,th,time,start)"
   'n)
 "period"
 "macro for extracting the period of signal a"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-duty-macro"
 '(".defmac duty_cycle(a,th,time)=(xdown(a,th,time,end)"
   "-xup(a,th,time,end))/(xdown(a,th,time,end)-xdown(a,th,time,start))*100"
   'n)
 "duty"
 "macro for extracting the duty cycle of signal a"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-settling-macro"
 '(".defmac settling(xaxis,a,ratio,Tstart,Tfinal)=xycond(xaxis,(a>(yval(a,Tfinal)*(1+ratio)))"
   " || (a<(yval(a,Tfinal)*(1-ratio))),Tfinal,Tstart) - Tstart"
   'n)
 "settling"
 "macro for extracting the settling cycle of signal A, within ±ratio of value of A at time Tfinal"
 'eldo-tempo-tags)

(tempo-define-template
 "eldo-subckt"
 '(".subckt "
   (p "[subckt name]: " lname) 'r 'n 'n
   ".ends " (s lname)  '>)
 "subckt"
 "template for inserting an Eldo subckt"
 'eldo-tempo-tags)



;;-----------------------------------------------------------------------------
;; Index menu (using `imenu.el') to create an index
;; of sections/subckt/models etc
;;-----------------------------------------------------------------------------

(defconst eldo-library-regexp-start
  "^\\s-*\\.\\(include\\|lib\\|libfas\\)\\s-+\\(?:key=\\w+\\s-\\)*'?"
  "Regexp that matches the beginning of library or include filename")

(defconst eldo-library-regexp-end
  "\\([^ \t\n']*\\)"
  "Regexp that matches the end of library or include filename")

(defconst eldo-misc-model-names
  '(
    "res" "r" "cap" "ind"
    "njf" "pjf" "nsw" "psw"
    "opa" "modfas" "logic" "a2d" "d2a"
    "macro"
    )
  "List of misc Eldo models types (no transistors)")

(defvar eldo-imenu-generic-expression
  (list
    (list
      "Sections"
      (concat eldo-section-regexp-start
          "\\s-+\\(.+\\)$") 1)
    (list
      "Misc"
      (concat
      "^\\s-*\\.model\\s-+"  eldo-model-name eldo-line-break
      "\\s-+\\("
      (regexp-opt eldo-misc-model-names)
      "\\)\\>") 1)
    (list
      "Libraries"
      (concat eldo-library-regexp-start
          eldo-library-regexp-end) 2)
    (list
      "Diodes"
      (concat "^\\s-*\\.model\\s-+"
          eldo-model-name eldo-line-break "\\s-+d\\>") 1)
    (list
      "Bipolars"
      (concat "^\\s-*\\.model\\s-+"
          eldo-model-name eldo-line-break "\\s-+\\(npn\\|pnp\\)\\>") 1)
    (list
      "Mosfets"
      (concat "^\\s-*\\.model\\s-+"
          eldo-model-name eldo-line-break "\\s-+\\(n\\|p\\)mos\\>") 1)
    (list
      "Analysis"
      (concat "^\\s-*\\.\\("
          (regexp-opt eldo-analysis-keywords)
          "\\)\\>") 1)
    (list
      "Testbenches"
      (concat
        "^\\s-*\\.define_testbench\\s-+\\([a-z]\\w+\\)") 1)
    (list
      "Tasks"
      (concat
        "^\\s-*\\.define_task\\s-+\\([a-z]\\w+\\)") 1)
    (list
      "Subckts"
      "^\\s-*\\.subckt\\s-+\\([a-z]\\w+\\)" 1)
  )
"Imenu generic expression for Eldo Mode.  See `imenu-generic-expression'.")


;; (defun eldo-index-function ()
;;   '(
;;     ("Devices"
;;         ("Bipolars" . 2)
;;         ("Diodes" . 3)
;;         ("Mosfets" . 4))
;;     ("Subckts" . 5)
;;     ("Sections" . 6)
;;     ("Testbenches" . 7)))


(defun eldo-imenu-init ()
  "Initialize index menu."
;;   (setq imenu-create-index-function #'eldo-index-function)
  (set (make-local-variable 'imenu-generic-expression) eldo-imenu-generic-expression)
  (set (make-local-variable 'imenu-case-fold-search) t)
  (set (make-local-variable 'imenu-sort-function) t)
  (set 'imenu-sort-function 'imenu--sort-by-name)
  (set (make-local-variable 'imenu-max-item_length) t)
  (imenu-add-to-menubar "Eldo-Index"))


;; loading of include files of current deck.
(defun eldo-load-include-files ()
  "Loads all files that are included in this deck. Makes it more easy
to load a project. This loading is not recursive. Files already
loaded are not reloaded or scanned for .includes. This function is
only guaranteed to work when all included files are not already loaded."
  (interactive)
  (eldo-load-include-files-recursively 't))

(defun eldo-load-include-files-recursively (&optional non-recursive)
  "Loads all files that are included in this deck. Makes it more easy
to load a project. This loading can occur recursively. Files already
loaded are not reloaded or scanned for .includes. This function is
only guaranteed to work when all included files are not already loaded."
  (interactive)
  (let ((index-alist (imenu--make-index-alist t))
        l filename)
    (if (setq l (cdr (assoc "Libraries" index-alist))) ;;file contains include files/libraries
        (while l
          (setq filename (expand-file-name
                          (substitute-in-file-name (car (car l)))))
          (message "Trying to load %s" filename)
          (if (and (file-readable-p filename)
                   (not
                    (assoc filename ;; already loaded
                           (mapcar
                            (lambda (buffer)
                              (cons (buffer-file-name buffer) buffer))
                            (buffer-list)))))
              (save-excursion
                (message "filename is %s" filename)
                (set-buffer (find-file-noselect filename))
                (unless non-recursive
                    (eldo-load-include-files-recursively))))
          (setq l (cdr l))))))

;; unloading of eldo files except current deck.
(defun eldo-unload-other-decks ()
  "Kills all other eldo files except current one. Makes it easy to
unload a lot of eldo files without restarting emacs."
  (interactive)
  (save-excursion
    (let ((current (current-buffer)))
      (mapcar
       (lambda (buffer)
     (set-buffer buffer)
     (if (and (eq major-mode 'eldo-mode)
          (not (eq current buffer)))
         (progn
           (message "Killing %s" buffer)
           (kill-buffer buffer))))
       (buffer-list)))))



;;-----------------------------------------------------------------------------
;; Eldo-mode starts here
;;-----------------------------------------------------------------------------
;;;###autoload
(defun eldo-mode ()
  "Eldo-mode is a major mode for highlighting ELDO netlist files.
Eldo is an analog simulator of Mentor (Siemens).
This mode requires the htmlize package.
(https://github.com/hniksic/emacs-htmlize)

Emacs-29 is well supported. For older versions of emacs, your
mileage may vary.
Turning on Eldo mode calls the value of the variable `eldo-mode-hook'
with no args, if that value is non-nil.

- COMMENTS:
Two types of comments are used:
- the '*' symbol is used to comment out a whole line - that symbol has
to be at the beginning of a line.
- the '!' symbol is used to document your netlist.


- DEFAULT TEMPLATE:
When you create a new file, a default template is inserted in it.

- TEMPLATES FOR MODELS, MACROMODELS AND WAVEFORMS:
A lot of templates are available. They are all listed in the 'Eldo' menu
('Add Element' 'Macros' and 'Extract' submenus).
You can also use them simply by hitting Ctrl-<TAB> after the appropriate tag.
For example, type 'M\\C-<tab>' at the beginning of a line and you will be prompted
with a complete Mosfet template. Most tags are pretty straightforward i.e 'C'
for a capacitor, 'L' for an inductance etc..., waveforms template are usually
triggred by the name i.e 'pulse\\C-<tab>' for a pulse.
You can type `C-h v tempo-tags' for a complete list of tags and associated templates.


- KEY BINDINGS:
\\{eldo-mode-map}


- HIGHLIGHTING (fontification):
By default, eldo-mode uses its own  set of faces for font-lock. You can modify these
by using the 'customize' package (group: eldo-mode) .
Eldo-mode will then use a custom set of font-lock faces to fontify your file.

- IMENU
Support for imenu is available in eldo-mode, imenu is activated by default if it is
available (this can be overridden with the 'customize package).
This allows you to have a list of subcircuits, models, testbenches, tasks and sections
in a dedicated menu ('Eldo-index').
Section names start with '!> '

- CUSTOMIZATION
Eldo-mode is customizable through the emacs 'customize' packages.
Customizable features are faces for font-locking, section headers and Imenu
activation.


To add automatic support put something like the following in your .emacs file:
  \(autoload 'eldo-mode \"eldo-mode\" nil  t\)
  \(setq auto-mode-alist \(cons '\(\"\\\\.cir\" . eldo-mode\) \
auto-mode-alist\)\)
  \(setq auto-mode-alist \(cons '\(\"\\\\.ckt\" . eldo-mode\) \
auto-mode-alist\)\)

This will trigger eldo-mode on all files that end in '.cir' or '.ckt' .
If your netlists file don't have a particular extension like '.cir'
you can try adding the following lines at the end of the file:

*** Local Variables:
*** mode:eldo
*** End:

This should trigger eldo-mode on that particular file when opened
(but don't forget to put:
    \(autoload 'eldo-mode \"eldo-mode\" nil  t\)
in your .emacs)

Copyright © 1997-2025 Emmanuel Rouat <emmanuel.rouat@st.com>"

  (interactive)
  (kill-all-local-variables)
  (setq tempo-interactive t)
  (setq mode-name "Eldo")
  (setq major-mode 'eldo-mode)
  (use-local-map eldo-mode-map)
  (setq case-fold-search 't)
  ;; Fix bloody tab shit!!
  (setq electric-indent-mode nil)             ;; never indent automatically
  (setq indent-tabs-mode nil)                 ;; only use spaces
  ;(setq tab-width 4)                         ;; leave this to user?
  (setq indent-line-function 'insert-tab)

  ;; support for comments
  (set (make-local-variable 'comment-start) "*")
  (set (make-local-variable 'comment-start-skip) "^[ \t]*\\*")
  (set (make-local-variable 'comment-end) "")
  (set (make-local-variable 'comment-padding) nil)
  (set (make-local-variable 'parse-sexp-ignore-comments) nil)
  (set (make-local-variable 'comment-multi-line) nil)

  ;; support for outline-mode (very limited)
  (set (make-local-variable 'outline-regexp) "\\(\\.subckt \\|\\.define_testbench \\|\\.define_task \\)")

  ;; support for auto-fill:
  (set (make-local-variable 'fill-prefix) "+ ")
  (set (make-local-variable 'auto-fill-inhibit-regexp) ".* !.* ")  ;; do not split 'doc' comments

  ;; Tempo tags - using 'tempo-local-tags' doesn't work (why??)
  (set (make-local-variable 'tempo-tags) (append eldo-tempo-tags tempo-tags))

  ;; syntax table table
  (set-syntax-table eldo-mode-syntax-table)

  ;; add Eldo mode menu
  (if (not eldo-menu-list)
      (setq eldo-menu-list (eldo-create-mode-menu)))
  (easy-menu-define eldo-menu eldo-mode-map
            "Menu keymap for Eldo Mode." eldo-menu-list)

  ;; if new file add a default template
  (if (= (buffer-size) 0) (eldo-file-initialize))

  ;; initialize imenu and which-function-mode
  (if eldo-use-imenu
      (if (fboundp 'imenu)
      (progn
        (eldo-imenu-init)
        (which-function-mode t)
      )))

  ;; force reformat netlist before saving buffer
  (add-hook 'before-save-hook 'eldo-reformat-netlist)

  ;; force imenu rescan when saving buffer
  (add-hook 'after-save-hook (lambda () (interactive)
                  (imenu--menubar-select imenu--rescan-item)))

  ;; font lock start-up
  (set (make-local-variable 'font-lock-defaults)
       (list 'eldo-font-lock-keywords nil t)) ;; nil -> t, don't do strings

  ; now hook in fontification
  (add-hook 'eldo-mode-hook 'font-lock-fontify-buffer)

  ;; now run eldo-mode-hook
  (run-hooks 'eldo-mode-hook))

(provide 'eldo-mode)



;;-----------------------------------------------------------------------------
;; CHANGELOG
;;-----------------------------------------------------------------------------
;;
;; Created:       June 97 (0.1)
;; Modified: September 97 (0.2)
;;           5 October 97 (0.3) - templates added
;;         20 November 97 (0.4) - comment-uncomment defun
;;         21 November 97 (0.5) - font names changed --> eldo-**-face
;;                                changed comment definition and regexps
;;               7 May 98 (0.6) - modifications in fonts definitions
;;                                (removed 'set-face-font ..' lines)
;;                                modified 'plot' regexp (added probe)
;;              13 May 98 (0.7) - created 'waveforms' submenu
;;                                added 'macromodels' submenu
;;                                and some macros templates
;;                                Corrected bug in comment regexp
;;              14 May 98 (0.8) - regexps simplified - fontification
;;                                seems faster now (?)
;;              6 July 98 (0.9) - small bug in assignment regexp
;;          29 August 98 (0.91) - added functions support
;;                                (copy-paste from spice-mode by C.J. Vieri)
;;        2 September 98 (0.92) - small bug fixes
;;       17 September 98 (0.93) - added 'extracts' templates
;;         23 October 98 (0.94) - added some 'extracts' templates
;;         30 October 98 (0.95) - added 'macro' templates
;;         28 January 99 (0.96) - added labels in 'extracts'
;;                                removed some buggy defmacs
;;            23 July 99 (0.97) - fixed buggy syntax table
;;          12 August 99 (0.98) - added 'changelog' support
;;                                added automatic template at startup
;;                                added abbrevs and auto-fill
;;                                expanded menu
;;        26 August 1999 (0.99) - fixed bug in file initialization
;;                                made dates Y2K compliant 8-)
;;                                added voltage/current source templates
;;                                added bunch of macromodels templates
;;                                modified default template and sections
;;                                fixed bug in changelog entry
;;       3 January 2000 (0.991) - fixed bug in fontification (order of
;;                                fontification is crucial)
;;                                added automatic loading of func-menu mode
;;      12 January 2000 (0.992) - auto-fill-mode not turned on automatically
;;                                (leave it to user)
;;                                default font-lock faces may be used now
;;                                simplified a few regexps
;;                                and made a very complicated one!! (assignments)
;;                                corrected syntax table
;;                                replaced '=' by '-' in comment-bar (recent versions
;;                                of eldo choke on '!=')
;;      18 January 2000 (0.993) - added automatic loading of libraries on
;;                                mouse-click
;;                                fixed bug with function-menu support
;;      24 January 2000 (0.994) - added support for paragraphs (useful??)
;;                                fixed bug in syntax table (#c #e comments)
;;      25 January 2000 (0.995) - fixed last bug with func-menu support (at last!!)
;;                                removed 'pos' variable (use 'let' instead)
;;                                made font-lock mode hooks local
;;                                made default value 'eldo-use-default-colors' true
;;        26 March 2000 (0.996) - added key bindings
;;                                made a few changes in colors
;;                                misc changes
;;                                exp. support for subckt names fontification
;;          26 May 2000 (0.997) - bugfix in support for subckt names fontification
;;         18 June 2000 (0.998) - added support for imenu
;;                                multi-line subckt fontification seems to work now
;;                                changed handling of comments
;;    14 September 2000 (0.999) - fixed multi-line subckt fontification with regexp
;;                                by Geert A. M. Van der Plas (works great! yahooo!)
;;                                added support for customization (group eldo-mode)
;;                                now use 'defface' for faces definition
;;                                added 'eldo-reformat' function
;;                                loading of libraries: now takes into account 'key=..'
;;                                use easymenu for menus now
;;                                looks like imenu support is fixed now
;;                                lots of cosmetic changes
;;                                added 'eldo-customize' function (from spice-mode)
;;                                now try to use usual font-lock faces, plus a few
;;                                 specific ones
;;   27 September 2000 (1.0-pre1)-release of 1.0-pre1
;;    07 November 2000 (1.0-pre2)-fixed bug in diodes imenu regexp
;;                                added multi-line support for imenu
;;                                added some vhdl-mode like key bindings
;;       05 April 2001 (1.0-pre3)-fixed loading of libraries using Geerts code
;;                                (use overlays instead of extents)
;;                                fixed eldo-model-name regexp
;;                                added eldo-mode-simulator-switches as defcustom
;;       10 April 2001 (1.0-pre4)-fixed loading of libraries using Geerts code
;;                                modified empty file initialization
;;                                some clean up in the file
;;       18 April 2001 (1.0-pre5)-implemented Geerts' sections support code
;;                                made function/variables naming consistent with the
;;                                'Emacs Lisp Coding Conventions'
;;                                fixed multi-line subckt fontification (AGAIN!)
;;                                corrected misspellings
;;               2 May 2001 (1.0)-release of 1.0
;;              21 May 2001 (1.1)-fixed regexp eldo-library-regexp-end
;;                                added 'ST' menu for manipulation of ST DK netlists
;;                                added 'settling' macro
;;                                added 'NETLIST' section header
;;                                added 'four' as analysis keyword
;;           April 2002 (1.2-pre)-added loading of 'include/lib' files (from spice-mode)
;;                                aligned imenu on spice-mode
;;                                aligned eldo-mode menu on spice-mode (more or less)
;;                                updated 'eldo-load-file-at-mouse' substitute function
;;                                updated templates to catch up on spice-mode
;;                                updated lots of macromodel templates
;;                                added 'hide comments' util (from spice-mode)
;;          April 2002 (1.2-pre3)-added loading of 'include/lib' files (from spice-mode)
;;                                modified 'gain margin' extract macro
;;                                added Geerts 'eldo-compile' function
;;                                changed 'library-regexp-start' for Emacs (G.vdP)
;;          April 2002 (1.2-pre4)-added Geerts 'search subckt def' feature (but made it
;;                                case insensitive)
;;                                took Geerts 'xinstance-regexp' - now fontifies Subckt/Model
;;                                names even if there is a commented out line in between
;;          April 2002 (1.2-pre5)-Test release
;;            15 April 2002 (1.2)-Release of 1.2
;;          16 April 2002 (1.2.1)-aligned eldo-mode fontification on spice-mode regarding
;;                                color of non '.' keywords starting line
;;               May 2002 (1.2.2)-updated 'satr' macromodel entry
;;                                corrected bug in 'settling' macro
;;            August 2002 (1.2.3)-added digital macromodel definitions
;;                                added mixed-signal macromodel definitions
;;                                removed abbrev-table definition
;;                                added 'ends' as command keyword (GvdP)
;;         September 2002 (1.2.4)-added switched cap macromodel definitions
;;                                added macromodel names ('Y' instances) fontification
;;                                made tempo-tags buffer-local (eldo-tempo-tags)
;;          February 2003 (1.2.5)-put 'checksoa' and 'conso' in analysis keywords
;;                                added 'nonoise' as keyword
;;                                added some sst keywords (eldo-RF)
;;                                improved doc-starter regexp (now handles global nets like
;;                                'VDD!' correctly)
;;                                removed parenthesis in 'noise' cource
;;                                added 'lstb' analysis
;;                                corrected 'inverter' template
;;                                added 'defhook'command and 'hook' model type for VHDL_AMS
;;               May 2004 (1.2.6)-put 'age' in analysis keywords
;;             April 2005 (1.2.7)-added ':' as word class caracter
;;                                added 'bind' as command
;;                                added 'iprobe' as command
;;          December 2005 (1.2.8)-added 'optimize' as analysis
;;                                added 'paramopt' as command
;;                                added 'monitor' as command
;;         September 2006 (1.2.9)-added 'objective'and 'printfile' as output
;;                                added 'correl' as command
;;          December 2006 (1.3.0)-added 'func' 'verilog'and 'disflat'  as command
;;         September 2007 (1.3.1)-added 'extmod'  as command
;;          November 2007 (1.3.2)-release on usenet
;;          November 2010 (1.3.3)-misc changes
;;              June 2011 (1.3.4)-added support for testbenches/tasks
;;            August 2012 (1.3.5)-added 'LOCALTOL' command
;;           January 2016 (1.3.6)-added 'plotwind' command
;;              June 2018 (1.3.7)-made file utf-8 clean
;;              June 2018 (1.3.8)-added 'histogram' command
;;              June 2022 (1.3.9)-removed some code that broke emacs and obsolete xemacs code
;;              June 2022 (1.4.0)-ffap-at-mouse works again (but filename is not highlighted
;;                                when mouse hovers over it.
;;          December 2022 (1.4.1)-added SOA section, modified section names (not full-caps anymore)
;;                                added 'discard' keyword
;;          February 2023 (1.4.2)-removed compilation mode
;;          November 2023 (1.4.3)-added 'dcmismatch' analysis
;;          February 2024 (1.4.4)-added 'tasks' in index
;;              June 2024 (1.4.5)-added face for tesbenches and tasks instances (DodgerBlue) - replaces 'warning' face
;;                                removed 'mprun' from commands
;;              June 2024 (1.4.6)-replaces tabs with spaces
;;                                removed last remaining xemacs code (kept original 'eldo-visit-subckt-def' commented in case...
;;                                removed (unnecessary?) ffap-at-mouse wrapper
;;          November 2024 (1.4.7)-added html-izing of buffer or region in eldo menu (needs htmlize.el package)
;;          December 2024 (1.4.8)-added fontification of expressions in curly brackets {}
;;                                changed section header to "!>" at beginning of line
;;                                force imenu rescan at saving of buffer
;;                                removed 'Go to Section' part (redundant with imenu)
;;                                made default header simpler
;;                                fixed buggy file initialization
;;                                removed speedbar stuff (does anyone use it?)
;;                                attempt to fix 'tab' indentation nonsense
;;                                changed some face colors
;;                                made imenu sorting alphabetical
;;    January/February 2025 (2.0)-removed paragraph support (didn't work anyway)
;;                                removed broken search-subckt stuff (use xref instead in future?)
;;                                improved 'tab' key behaviour
;;                                rewrote 'comment' functions for saner behaviour
;;                                renamed 'eldo-comment-bar' to 'eldo-section-bar'
;;                                regrouped some functions more logically
;;                                turn 'which-function-mode' on if imenu is enabled
;;                                do not insert comment symbol ('*') if line is empty when commenting section
;;                                force 'reformat-netlist before saving buffer
;;                                removed unused 'eldo-circuit-header' tempo and 'eldo-default-header' variable
;;                                removed broken 'hide/unhide comments code'
;;                                fixed 'doc comments' not being commented (and made '!' a word in syntax table)
;;                                fixed 'sections' regexp (section is not seen if commented out)
;;                                added 'units' and 'operators' fontification
;;                                added function that attempts tempo-completion, indents if it fails
;;                                'return' key now indents next line at same level
;;                                removed 'Mismatch' menu (easy to do by hand anyway)
;;                                removed predefined sections tools. Just use sections + imenu
;;                                added code by A. Buchet for subckt (un)folding and comment hiding (really cool)
;; eldo-mode.el ends here
;; kate: syntax lisp


